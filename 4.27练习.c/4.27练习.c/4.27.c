#define _CRT_SECURE_NO_WARNINGS 1
////////1. 判断一个数是否为奇数
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	scanf("%d", &i);
//	if (i % 2 != 0)
//	{
//		printf("%d是奇数", i);
//	}
//	else
//	{
//		printf("%d不是奇数", i);
//	}
//	return 0;
//}


//////输出1 - 100之间的奇数
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 101; i++)
//	{
//		if (i % 2 != 0)
//		{
//			printf("%d\t", i);
//		}
//	}
//	return 0;
//}

//////利用函数输出1 - 100之间的奇数
//#include<stdio.h>
//int judge(int i)
//{
//	if (i % 2 != 0)
//	{
//		return 0;
//	}
//	return 1;
//}
//int main()
//{
//	int i = 0;
//	for (i = 1; i < 101; i++)
//	{
//		int ret = judge(i);
//		if (ret==0)
//		{
//			printf("%d\t", i);
//		}
//	}
//	return 0;
//}

////////////验证省略for循环初始化打印次数
////#include<stdio.h>
////int main()
////{
////    int i = 0;
////    int j = 0;
////    int count = 0;
////    for (; i < 10; i++)
////    {
////        for (; j < 10; j++)
////        {
////            printf("hehe\n");
////            count++;
////        }
////    }
////    printf("%d", count);
////    return 0;
////}


////#include<stdio.h>
////int main()
////{
////    int i = 0;
////    int j = 0;
////    int count = 0;
////    for (i = 0; i < 10; i++)
////    {
////        for (j = 0; j < 10; j++)
////        {
////            printf("hehe\n");
////            count++;
////        }
////    }
////    printf("%d", count);
////    return 0;
////}

//////#include <stdio.h>
//////int main()
//////{
//////    int i = 1;
//////    do
//////    {
//////        printf("%d ", i);
//////        i = i + 1;
//////    } while (i <= 10);
//////    return 0;
//////}
//////

//#include <stdio.h>
//int main()
//{
//    int i = 1;
//    do
//    {
//        if (5 == i)
//            break;
//        printf("%d ", i);
//        i = i + 1;
//    } while (i <= 10);
//
//    return 0;
//}
// 
// 
// 
// 
//#include <stdio.h>
//int main()
//{
//    int i = 1;
//
//    do
//    {
//        if (5 == i)
//            continue;
//        printf("%d ", i);
//        i = i + 1;
//    } while (i <= 10);
//
//    return 0;
//}


//////////1. 计算 n的阶乘。
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int ret = 1;
//	int q = 0;
//	scanf("%d", &q);
//	for (i = 1; i <= q; i++)
//	{
//		ret=ret*i;
//	}
//	printf("%d的阶乘是%d",q,ret);
//	return 0;
//}



////2. 计算 1!+ 2!+ 3!+ …… + 10!
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int ret = 1;
//	int sum = 0;
//	for (i = 1; i <= 10; i++)
//	{
//		ret = ret * i;
//		sum = sum + ret;
//	}
//	printf("%d",sum);
//	return 0;
//}


////////3. 在一个有序数组中查找具体的某个数字n。（讲解二分查找）
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int k = 0;
//	int right = sz - 1;
//	int left = 0;
//	int flag = 0;
//	scanf("%d",&k);
//	while (left<=right)
//	{
//		int mid = left + (right - left) / 2;
//		if (arr[mid] == k)
//		{
//			printf("找到了下标为：%d", mid);
//			flag = 1;
//			break;
//		}
//		else if (k> arr[mid])
//		{
//			left = mid + 1;
//		}
//		else
//		{
//			right = mid - 1;
//		}
//	}
//	if (flag == 0)
//	{
//		printf("找不到");
//	}
//	return 0;
//}

////4. 编写代码，演示多个字符从两端移动，向中间汇聚。
////#include<stdio.h>
////#include<string.h>
////#include<Windows.h>
////int main()
////{
////	char ch1[] = { "*******************************" };
////	char ch2[] = { "hello                     world" };
////	int left = 0;
////	int sz = strlen(ch1);
////	int right =sz-1;
////	printf("%s", ch1);
////	while (left <= right)
////	{
////		Sleep(600);
////		system("cls");
////		ch1[left] = ch2[left];
////		ch1[right] = ch2[right];
////		printf("%s\n", ch1);
////		left++;
////		right--;
////	}
////}

////////编写代码实现，模拟用户登录情景，并且只能登录三次。（只允许输入三次密码，如果密码正确则
////////提示登录成，如果三次均输入错误，则退出程序。


















//////递归的介绍
//////#include<stdio.h>
//////int Fun(int n);
//////int Fun(int n)
//////{
//////    if (n == 5)
//////        return 2;
//////    else
//////        return 2 * Fun(n + 1);
//////}
//////int main()
//////{
//////    int ret = Fun(2);
//////    printf("%d", ret);
//////    return 0;
//////}


//验证printf函数的特性
//#include<stdio.h>
//int main()
//{
//	printf("%d ", printf("%d ", printf("%d ", 43)));
//
//	return 0;
//}