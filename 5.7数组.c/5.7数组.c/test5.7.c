#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>


//查看数组在内存中的分布
//int main()
//{
//	int arr[] = {1,2,3,4,5,6,7,8,9,10};
//	char arr2[] = "abcdefg";
//	int i = 0;
//	int q = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%p\n", &arr[i]);
//	}
//	printf("\n");
//	for (q = 0; q < 10; q++)
//	{
//		printf("%p\n", &arr2[q]);
//	}
//
//
//	return 0;
//}


////查看""字符串内部结构与{}内部结构，""内部直接自己跟上\0
////int main()
////{
////	char arr[] = "abc";
////	char arr2[] ={'a','b','c'};
////	printf("%d\n", sizeof(arr));
////	printf("%d", sizeof(arr2));
////
////	return 0;
////}


								////实现一个对整形数组的冒泡排序
//进行n-1组排序，然后再进行n-1-i次内部的排序
//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	int j = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	printf("数组元素为：");
//	for (i = 0; i < sz; i++)
//	{
//
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//	for (i = 0; i < sz - 1; i++)
//	{
//		int flag = 0;
//		int tem = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] < arr[j + 1])
//			{
//				tem = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tem;
//			}
//			flag = 1;
//		}
//		if (flag == 0)
//			break;
//	}
//	printf("排序后数组为 ：");
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

						//···将数组A中的内容和数组B中的内容进行交换。（数组一样大）
				/*非函数解决*/
//int main()
//{
//	int arr1[5] = { 1,2,3,4,5 };
//	int arr2[5] = { 6,7,8,9,10};
//	int tem[5] = { 0 };
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		tem[i] = arr1[i];
//	}
//	for (i = 0; i < 5; i++)
//	{
//		arr1[i] = arr2[i];
//	}
//	for (i = 0; i < 5; i++)
//	{
//		arr2[i] = tem[i];
//	}
//	printf("交换后的数组为：\n");
//	for (i = 0; i < 5; i++)
//	{
//		printf("arr1[%d]=%d\t", i, arr1[i]);
//	}
//	printf("\n");
//	for (i = 0; i < 5; i++)
//	{
//		printf("arr2[%d]=%d\t", i, arr2[i]);
//	}
//	return 0;
//}








							/*利用函数交换两个数组*/
//void changearr(int arr1[], int arr2[], int tem[])
//{
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		tem[i] = arr1[i];
//	}
//	for (i = 0; i < 5; i++)
//	{
//		arr1[i] =arr2[i];
//	}
//	for (i = 0; i < 5; i++)
//	{
//		arr2[i] = tem[i];
//	}
//}
//int main()
//{
//	int arr1[5] = { 1,2,3,4,5 };
//	int arr2[5] = { 6,7,8,9,10 };
//	int tem[5] = { 0 };
//	int i = 0;
//	changearr(arr1, arr2, tem);
//	printf("交换后的数组为：\n");
//	for (i = 0; i < 5; i++)
//	{
//		printf("arr1[%d]=%d\t", i, arr1[i]);
//	}
//	printf("\n");
//	for (i = 0; i < 5; i++)
//	{
//		printf("arr2[%d]=%d\t", i, arr2[i]);
//	}
//	return 0;
//}



			////创建一个整形数组，完成对数组的操作
////
////实现函数init() 初始化数组为全0
////实现print()  打印数组的每个元素
////实现reverse()  函数完成数组元素的逆置。
////要求：自己设计以上函数的参数，返回值。
void init(int arr1[5],int sz1)
{
	int i = 0;
	for (i = 0; i < sz1; i++)
	{
		arr1[i] = 0;
	}
}
void print(int arr[5],int sz,char str[])
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%s[%d]=%d\t",str, i, arr[i]);
	}
}
void reverse(int arr2[5],int sz2)
{
	int left = 0;
	int tem = 0;
	int right = sz2 - 1;
	while (left < right)
	{
		 tem= arr2[right];
		arr2[right] = arr2[left];
		arr2[left] = tem;
		left++;
		right--;
	}
}
int main()
{
	char str1[] = "arr1";
	char str2[] = "arr2";
	int arr1[5] = { 5,6,7,8,9 };
	int arr2[5] = { 1,2,3,4,5 };
	int sz1 = sizeof(arr1) / sizeof(arr1[0]);
	int sz2 = sizeof(arr2) / sizeof(arr2[0]);
	init(arr1, sz1);
	printf("初始数组1如下：\n");
	print(arr1, sz1,str1);
	printf("\n");
	reverse(arr2, sz2);
	printf("数组2逆置后数组为：\n");
	print(arr2, sz2,str2);
	return 0;
}