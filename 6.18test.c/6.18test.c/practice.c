#define _CRT_SECURE_NO_WARNINGS 1


//利用数组进行求解  BC46 判断是元音还是辅音
//#include<stdio.h>
//int main()
//{
//    char arr[10] = { 'a','e','i','o','u','A','E','I','O','U' };
//    char ch = 0;
//    int tem = 0;
//    while (~scanf("\n%c", &ch))
//    {
//        if (ch != '\0')
//        {
//            for (int i = 0; i < 10; i++)
//            {
//                if (arr[i] == ch)
//                {
//                    tem = 10;
//                    break;
//                }
//                else
//                {
//                    tem = 20;
//                }
//            }
//        }
//        if (tem == 10)
//        {
//            printf("Vowel\n");
//        }
//        else
//        {
//            printf("Consonant\n");
//        }
//    }
//    return 0;
//}




//BC47 判断是不是字母
//#include <stdio.h>
//int main()
//{
//    char h = 0;
//    int s = 0;
//    while (scanf("%c", &h) != EOF)
//    {
//        s = h;
//        if (h != '\n')
//        {
//            if (((s >= 65) && (s <= 90)) || ((s >= 97) && (s <= 122)))
//            {
//                printf("%c is an alphabet.\n", h);
//                continue;
//            }
//            else
//            {
//                printf("%c is not an alphabet.\n", h);
//                continue;
//            }
//        }
//    }
//    return 0;
//}


//BC48 字母大小写转换


//#include<stdio.h>
//int main()
//{
//    char ch = 0;
//    while (~scanf("\n%c", &ch))
//    {
//        if (ch >= 'A' && ch <= 'Z')
//        {
//            printf("%c\n", ch + 32);
//        }
//        else if (ch >= 'a' && ch <= 'z')
//        {
//            printf("%c\n", ch - 32);
//        }
//    }
//    return 0;
//}

//利用islower函数进行求解，需引用头文件#include<ctype.h>!

//#include <ctype.h>
//#include<stdio.h>
//#include<nl_types.h>
//int main()
//{
//    char ch = 0;
//    while (~scanf("\n%c", &ch))
//    {
//        if (islower((int)ch)) //islower函数需要引用头文件#include<ctype.h>
//
//        {
//            printf("%c\n", ch - 32);  //若为小写字母则返回非0，大写字母为0！
//        }
//        else
//        {
//            printf("%c\n", ch + 32);
//        }
//    }
//    return 0;
//}

//BC49 判断两个数的大小关系

//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    int b = 0;
//    while (~scanf("\n%d%d", &a, &b))
//    {
//        if (a > b)
//        {
//            printf("%d>%d\n", a, b);
//        }
//        else if (a == b)
//        {
//            printf("%d=%d\n", a, b);
//        }
//        else
//        {
//            printf("%d<%d\n", a, b);
//        }
//    }
//    return 0;
//
//}

//BC50 计算单位阶跃函数


//#include<stdio.h>
//int main()
//{
//    float  i = 0.0f;
//    while (~scanf("\n%f", &i))
//    {
//        if (i > 0)
//        {
//            i = 1;
//            printf("1\n");
//        }
//        else if (i == 0.0)
//        {
//            printf("0.5\n");
//        }
//        else
//        {
//            printf("0\n");
//        }
//    }
//    return 0;
//}
//判断是否为三角形！



////#include <stdio.h>
//int main()
//{
//    int a = 0;
//    int b = 0;
//    int c = 0;
//    while (~scanf("\n%d%d%d", &a, &b, &c))
//    {
//        if (a + b > c && a + c > b && b + c > a)
//        {
//            if (a == b && b == c)
//            {
//                printf("Equilateral triangle!\n");
//            }
//            else if (a == b || a == c || b == c)
//            {
//                printf("Isosceles triangle!\n");
//
//            }
//            else
//            {
//                printf("Ordinary triangle!\n");
//            }
//        }
//        else
//        {
//            printf("Not a triangle!\n");
//        }
//    }
//    return 0;
//}


//BC52 衡量人体胖瘦程度


//#include<stdio.h>
//#include <sys/types.h>
//int main()
//{
//    int w = 0;
//    int h = 0;
//    while (~scanf("\n%d%d", &w, &h))
//    {
//        float b = w / ((h / 100.0) * (h / 100.0));
//        if (b < 18.5)
//        {
//            printf("Underweight\n");
//
//        }
//        else if (b >= 18.5 && b <= 23.9)
//        {
//            printf("Normal\n");
//        }
//        else if (b > 23.9 && b <= 27.9)
//        {
//            printf("Overweight\n");
//        }
//        else
//        {
//            printf("Obese\n");
//        }
//    }
//    return 0;
//}