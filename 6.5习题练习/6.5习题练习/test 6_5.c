#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//输入一个整数数组，实现一个函数，
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//所有偶数位于数组的后半部分。
//1 2 3 4 5 6 7 8 9
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int left = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int right = sizeof(arr) / sizeof(arr[0])-1;
//	while (left<right)
//	{
//
//		//从左向右找一个偶数！把他与后面的一个奇数调换位置即可
//		while (left < right && arr[left] % 2 == 1)		//left<right这个条件是为了防止该数列全是奇数，导致数组越界访问！
//		{
//			left++;
//		}
//		//从右往左找一个奇数，把他与前面那个偶数的位置进行调换即可
//		while (right > left && arr[right] % 2 == 0)    //left<right这个条件是为了防止该数列全是偶数，导致数组越界访问！
//		{
//			right--;
//		}
//		//只有当两个数不相同时才进行调换！
//		if (arr[left] != arr[right])
//		{
//			int tem = arr[left];
//			arr[left] = arr[right];
//			arr[right] = tem;
//		}
//	}
//	for (int j = 0; j < sz; j++)
//	{
//		printf("%d ", arr[j]);
//	}
//	return 0;
//}





//日本某地发生了一件谋杀案，警察通过排查确定杀人凶手必为4个嫌疑犯的一个。
//以下为4个嫌疑犯的供词:
//A说：不是我。
//B说：是C。
//C说：是D。
//D说：C在胡说
//已知3个人说了真话，1个人说的是假话。
//现在请根据这些信息，写一个程序来确定到底谁是凶手。

//int main()
//{
//	char killer = 'a';    //首先假设杀手为a！
//	for (killer = 'a'; killer <= 'e'; killer++)   //然后杀手进行遍历！abcd均可能为杀手！
//	{
//		if ((killer != 'a') + (killer == 'c') + (killer == 'd') + (killer != 'd') == 3)//只有最后结果为3才能得出答案！
//		{
//			printf("%c", killer);   //最后打印出来杀手即可！
//		}
//	}
//}


//5位运动员参加了10米台跳水比赛，有人让他们预测比赛结果：
//A选手说：B第二，我第三；
//B选手说：我第二，E第四；
//C选手说：我第一，D第二；
//D选手说：C最后，我第三；
//E选手说：我第四，A第一；
//比赛结束后，每位选手都说对了一半，请编程确定比赛的名次。
int main()
{
	int a = 0;
	int b = 0;
	int c = 0;
	int d = 0;
	int e = 0;
	for (a = 1; a <= 5; a++)
	{
		for (b = 1; b <= 5; b++)
		{
			for (c = 1; c <= 5; c++)
			{
				for (d = 1; d <= 5; d++)
				{
					for (e = 1; e <= 5; e++)
					{
						if ((b == 2 || a == 3) +
							(b == 2 || e == 4) +
							(c == 1 || d == 2) +
							(c == 5 || d == 3) +
							(e == 4 || a == 1) == 5)
							{
								if (a * b * c * d * e == 120 && a + b + c + d + e == 15)
								{
									printf("a=%d ,b=%d ,c=%d ,d=%d ,e=%d ", a, b, c, d, e);
								}
							}	
					}
				}
			}
		}
	}
}

//int main()
//{
//    int a = 0;
//    int b = 0;
//    int c = 0;
//    int d = 0;
//    int e = 0;
//    for (a = 1; a <= 5; a++)
//    {
//        for (b = 1; b <= 5; b++)
//        {
//            for (c = 1; c <= 5; c++)
//            {
//                for (d = 1; d <= 5; d++)
//                {
//                    for (e = 1; e <= 5; e++)
//                    {
//                        if (((b == 2) + (a == 3) == 1) &&
//                            ((b == 2) + (e == 4) == 1) &&
//                            ((c == 1) + (d == 2) == 1) &&
//                            ((c == 5) + (d == 3) == 1) &&
//                            ((e == 4) + (a == 1) == 1))
//                        {
//                            if (a * b * c * d * e == 120)
//                                printf("a=%d b=%d c=%d d=%d e=%d\n", a, b, c, d, e);
//                        }
//                    }
//                }
//            }
//        }
//    }
//    return 0;
//}
