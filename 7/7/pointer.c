#define _CRT_SECURE_NO_WARNINGS 1
		//7.6指针作业！
//有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，
//请编写程序在这样的矩阵中查找某个数字是否存在。
//要求：时间复杂度小于O(N);

#include<stdio.h>
//void find(int arr[][3], int row, int col, int input)
//{
//	int i = 0;
//	int j = col - 1;
//	int flag = 0;
//	while (i < row && j >=0)
//	{
//		if (arr[i][j] == input)
//		{
//			printf("找到了，数组下标为arr[%d][%d]", i, j);
//			flag = 1;
//			break;
//		}
//		else if(input>arr[i][j])
//		{
//			i++;
//		}
//		else if (input < arr[i][j])
//		{
//			j--;
//		}
//	}
//	if (flag == 0)
//	{
//		printf("找不到该元素！\n");
//	}
//}
//int main()
//{
//	int arr[3][3] = { 1,2,3,2,3,4,3,4,5 };
//	int input = 0;
//	scanf("%d", &input);
//	find(arr, 3, 3, input);
//	return 0;
//}
	//测试sizeof计算值！
////#include<stdio.h>
////int main()
////{
////	char arr[] = { 'a','c','b'};
////	printf("%d", sizeof(arr));
////	return 0;
////
////}



//变种水仙花数 - Lily Number：把任意的数字，从中间拆分成两个数字，比如1461
//可以拆分成（1和461）, （14和61）, （146和1), 如果所有拆分后的乘积之和等于自身，则是一个Lily Number。
//例如：
//655 = 6 * 55 + 65 * 5
//1461 = 1 * 461 + 14 * 61 + 146 * 1
//求出 5位数中的所有 Lily Number。
//#include<math.h>
//int main()
//{
//	int i = 10000;
//	for (i = 10000; i < 99999; i++)
//	{
//		int n = 0;
//		int sum = 0;
//		for (n =1; n<=4; n++)
//		{
//			int ret = pow(10, n);
//			sum += (i/ret) * (i %ret);
//		}
//		if (sum == i)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}


//实现一个函数，可以左旋字符串中的k个字符。
//例如：
//ABCD左旋一个字符得到BCDA
//左旋即第一个移动到最后一个，其他依次往前移动一个！
//ABCD左旋两个字符得到CDAB			
//  A  B  C  D                      
//BA   DC     
// CDAB
// 观察可以发现，若改变的字符个数为n个时，
//首先让前n个元素交换位置，
// 其次让剩余倒置一下,最后再整体倒置一下即可
// 在数组改变了其位置，最后打印出数组的每一个元素！
//void swap(char arr[], int left, int right)
//{
//	while (left <= right)
//	{
//		char tem = arr[left];
//		arr[left] = arr[right];
//		arr[right] = tem;
//		left++;
//		right--;
//	}
//}
//void Swap(char arr[],int n, int left,int right)
//{
//	int tem = right;
//	//right = n;
//	swap(arr, 0, n-1);
//	left = n ;
//	right = tem;
//	swap(arr, n , tem);
//	swap(arr, 0, tem);
//	int i = 0;
//	for (i = 0; i <= right; i++)
//	{
//		printf("%c",arr[i]);
//	}
//}
//仅仅使用了投机的方法，实质上未改变数组中的位置！
//void print(char arr[], int sz, int q)
//{
//	int i = 0;
//	for (i = q; i < sz; i++)
//	{
//		printf("%c", arr[i]);
//	}
//	for (i = 0; i < q; i++)
//	{
//		printf("%c", arr[i]);
//	}
//}
//int main()
//{
//	char arr[4] = {'a','b','c','d'};
//	int sz = sizeof(arr) / sizeof(*arr);
//	int q = 0;
//	int left = 0;
//	int right = sizeof(arr) / sizeof(*arr) - 1;		//数组中最后一个元素的下标！
//	scanf("%d", &q);
//	/*print(arr, sz,q);*/
//	Swap(arr,q, left, right);
//	return 0;
//}

