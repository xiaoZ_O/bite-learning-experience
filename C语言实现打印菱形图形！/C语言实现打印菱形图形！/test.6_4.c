#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//实现菱形的打印，首先分为两部分！
//上半面和下半面！
//上半面的* 号从第零行开始有一个，然后下面每行一次 + 2；  2n + 1;
//        空格从第零行开始有								上半面行数-i-1；


int main()
{
	int n = 0;
	scanf("%d", &n);
	//打印上半部分！
	for (int i = 0; i < n; i++)
	{
		//打印空格
		for (int j = 0; j < n-1-i; j++)
		{
				printf(" ");
		}
		//打印*号
		for (int j = 0; j < 2 * i + 1; j++)
		{
			printf("*");
		}
		printf("\n");
	}
	//下半部分！			//下半部分比上半部分少一行！
	
	for (int i = 0; i < n - 1; i++)
	{
		int j = 0;
		//下半部分的空格打印！
		//下半部分的空格数从1开始以此递增！
		//下半部分的*，从
		for (int j = 0; j <= i; j++)
		{
			printf(" ");
		}
		for (int j = 0; j < 2 * (n - 1-i) - 1; j++)
		{
			printf("*");
		}
		printf("\n");
	}
	return 0;

}