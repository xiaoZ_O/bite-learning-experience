#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
struct stu
{
	char name[20];
	int age;
};

				//打印函数！
void print(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}


			//整形比较函数！
int cmp_int(const void* e1, const void* e2,int width)
{
	return *(int*)e1 - *(int*)e2;
}

		//结构体中的数字比较
int cmp_byage(const void* e1, const void* e2, int width)
{
	return ((struct stu*)e1)->age - ((struct stu*)e2)->age;
}

		//结构体中的字母进行比较!			
int cmp_name(const void* e1, const void* e2, int width)
{
	return strcmp(((struct stu*)e1)->name , ((struct stu*)e2)->name);
}



		//用于交换的函数！
void Swap(char* e1,  char* e2, int width)
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char tem = *e1;
		*e1 = *e2;
		*e2 = tem;
		e1++;
		e2++;

	}
}


		//使用冒泡思想模拟实现qsort
void bubble_sort(void *base,int sz,
	int width, void cmp(const void* e1, const void* e2))
{
	int i = 0;
	int j = 0;
	for (i = 0; i < sz - 1; i++)
	{
		for (j = 0; j < sz - 1 - i; j++)
		{
			if (cmp_name((char*)base + j * width,(char*)base + (j + 1) * width,width)>0)
			{
				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
			}
		}
	}
}

		//排序整形数组！
//void test1()
//{
//	//自己利用冒泡思想来实现一个和qsort函数功能一样的排序函数！
//	int arr[10] = { 1,2,4,3,0,5,8,6,7,9 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz, sizeof(arr[0]), cmp_int);
//	print(arr, sz);
//}





		//排序结构体(根据年龄）
//void test2()
//{
//	struct stu s[3] = { {"zhangsan",20},{"lisi",30},{"pengge",25}};
//	int sz = sizeof(s) / sizeof(s[0]);
//	bubble_sort(s, sz, sizeof(s[0]),cmp_byage);
//}

		//排序结构体（根据字母）
	void test3()
	{
		struct stu s[3] = { {"zhangsan",20},{"lisi",30},{"pengge",25} };
		int sz = sizeof(s) / sizeof(s[0]);
		bubble_sort(s, sz, sizeof(s[0]), cmp_name);
	}


int main()
{
	/*test1();*/
	/*test2();*/
	test3();
	
}