#define _CRT_SECURE_NO_WARNINGS 1
//模拟计算器的实现！
#include<stdio.h>
int Add(int x, int y)
{
	return x + y;
} 

int Sub(int x, int y)

{
	return x - y;
}

int Mul(int x, int y)
{
	return x * y;
}

int Div(int x, int y)
{
	return x / y;
}

//一、利用函数指针来实现计算器！，减少了冗余！
//void Cal(int(*p)(int x, int y))
//{
//	int x = 0;
//	int y = 0;
//	printf("请输入两个操作数:");
//	scanf("%d%d", &x, &y);
//	int ret = p(x,y);
//	printf("%d\n", ret);
//}
void menu()
{
	printf("**********************\n");
	printf("**1.Add*****2.Sub*****\n");
	printf("**3.Mul*****4.Div*****\n");
	printf("******0.exit**********\n");
}

//int main()
//{
//	int x = 0;
//	int y = 0;
//	int input = 0;
//	do
//	{
//		menu();
//		printf("请选择->");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			Cal(Add);
//			break;
//		case 2:Cal(Sub); break;
//		case 3:Cal(Mul); break;
//		case 4:Cal(Div); break;
//		case 0:printf("退出计算器\n"); break;
//		default:printf("输入错误，请重新选择！\n"); break;
//		}
//	} while (input);
//}




//二、利用函数指针数组来实现计算器，使得case使用更加方便！容易再添加新的功能！
//int main()
//{
//	int input = 0;
//
//	do
//	{
//		menu();
//		printf("请进行选择->");
//		scanf("%d", &input);
//		int x, y;
//		if (input >= 1 && input <= 4)
//		{
//			int (*parr[5])(int x, int y) = { 0,Add,Sub,Mul,Div };
//			printf("请输入两个操作数:");
//			scanf("%d%d", &x, &y);
//			int ret = parr[input](x, y);
//			printf("%d\n", ret);
//		}
//		else if (input == 0)
//		{
//			printf("退出计算器！\n");
//			//break;
//		}
//		else
//		{
//			printf("输入错误，请重新选择:\n");
//		}
//	} while (input);
//	return 0;
//}