#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#define ROW 9					/*用来设定扫雷的行数与列数*/
#define COL 9

#define ROWS ROW+2				/*防止越界访问所以将整个数组上下左右都加1；*/
#define COLS COL+2


#define les 10

			/*初始化数组*/
void Initboard(char board[ROWS][COLS], int rows, int cols,char ret);
			/*打印出数组*/
void Display(char board[ROWS][COLS], int rows, int cols);
			/*布置雷*/
void Setmine(char board[ROWS][COLS], int row, int col);
			/*用来表示周围雷的个数*/
int Showmines(char mine[ROWS][COLS], char check[ROWS][COLS], int x, int y);
			/*找雷函数，若是雷则被炸死，否则在此坐标写上附近雷的个数*/
void Findmine(char mine[ROWS][COLS], char check[ROWS][COLS], int rows, int cols);