#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"
//首先先打印游戏界面。
//然后进行选择，进入游戏后，进入game函数内部。
//首先创建两个字符数组，一个用来存放地雷的排布，
//另一个用来存放地雷范围图。
//然后进行初始化数组，在mine数组中全部初始化为0,0代表非雷，1代表雷；
// 开始在数组中布置雷，假设9*9中布置10雷，
//					check中全部初始化为*，一旦发现附近有雷，则用数字字符代替。

//注：将雷的个数设置为90个，可以快速检测代码是否正确！
//正常游戏的时候将雷设置10个左右即可！
void game()
{
	char mine[ROWS][COLS] = { 0 };				/*mine数组用来存放地雷分布的一个字符数组。*/
	char check[ROWS][COLS] = { 0 };				/*check数组用来存放附近地雷个数的字符数组。*/
	Initboard(mine, ROWS, COLS, '0');			/*初始化用来放置雷的棋盘*/
	Initboard(check, ROWS, COLS, '*');			/*初始化用来存储附近雷的个数的棋盘*/
	/*Display(mine, ROW, COL);*/							/*展示*/
	Display(check, ROW, COL);							/*展示*/
	Setmine(mine, ROW, COL);					/*布置雷*/
	/*Display(mine, ROW, COL);*/					/*展示雷所在的位置*/
	Findmine(mine, check, ROW, COL);
	



}
void menu()
{
	printf("**********************\n");
	printf("*******1.play*********\n");
	printf("*******0.exit*********\n");
	printf("**********************\n");
}
int main()
{

	srand((unsigned int)time(NULL));
	int input = 0;
	do
	{
		menu();
		printf("请进行选择：\n");
		scanf("%d", &input);
		switch (input)
		{
		case 1:game();
			break;
		case 0:printf("退出游戏\n");
			break;
		default:printf("输入错误,请重新选择\n");
			break;
		}
	} while (input);
	return 0;
}