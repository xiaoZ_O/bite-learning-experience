#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>


//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
//例如：给定s1 = AABCD和s2 = BCDAA，返回1
//给定s1 = abcd和s2 = ACBD，返回0.
//AABCD左旋一个字符得到ABCDA
//AABCD左旋两个字符得到BCDAA
//AABCD右旋一个字符得到DAABC
//int main()
//{
//	char arr[10] = { 0 };
//	gets(arr);
//	char tem[100] = { 0 };
//	strcpy(tem, arr);
//	strcat(tem, arr);
//	char* ret = strstr(tem, arr);
//	if (ret == NULL)
//	{
//		printf("不是\n");
//	}
//	else
//	{
//		printf("1");
//	}
//	return 0;
//}
//

//使用冒泡排序完成qsort函数内部的实现！
void Swap(char* e1, char* e2, int width)
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char tem = *e1;
		*e1 = *e2;
		*e2 = tem;
		e1++;
		e2++;
	}
}
void bubble_sort(void* base, int number, int width, int cmp(void* e1, void* e2))
{
	int i = 0;
	int j = 0;
	for (i = 0; i < number - 1; i++)
	{
		for (j = 0; j < number - i - 1; j++)
		{
			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
			{
				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
			}
		}
	}
}




////排序整形！
int cmp_int(const void*e1,const void *e2)
{
	return *(int*)e1 - *(int*)e2;
}
void test1()
{
	int arr[10] = { 6,5,4,3,2,1,0,7,8,9 };
	int sz = sizeof(arr) / sizeof(*arr);
	bubble_sort(arr, sz, sizeof(arr[0]), cmp_int);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}
////排序结构体！
struct stu
{
	char name[10];
	int age;
};

////结构体中以年龄排序！
int cmp_byage(const void* e1, const void* e2)
{
	return ((struct stu*)e1)->age - ((struct stu*)e2)->age;
}
//
////结构体中以名字排序！
int cmp_byname(const void* e1, const void* e2)
{
	return strcmp(((struct stu*)e1)->name, ((struct stu*)e2)->name);
}
void test2()
{
	struct stu s[3] = { {"zhangsan ",15},{"lisi ",30},"wangw",25 };
	int sz = sizeof(s) / sizeof(*s);
	qsort(s, sz, sizeof(*s), cmp_byage);
	//qsort(s, sz, sizeof(*s), cmp_byname);
}

int main()
{
	/*test1();*/
	test2();
}



