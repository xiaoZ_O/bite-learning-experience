
//创建打印乘法口诀的函数
#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//void prin(int q);
//void prin(int q)
//{
//	int i = 0, j = 0;
//	for (i = 1; i <= q; i++)
//	{
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%d\t", i, j, i * j);
//
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	int q = 0;
//	scanf("%d", &q);
//	prin(q);
//	return 0;
//}



////					利用函数交换两个数的值
//#include<stdio.h>
//void tem(int* a, int* b);
//void tem(int* a, int* b)
//{
//	int ret =0;
//	ret = *a;
//	*a = *b;
//	*b = ret;
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	tem(&a, &b);
//	printf("%d %d ", a, b);
//	return 0;
//}



				//利用二分法找元素
//#include<stdio.h>
//int main()
//{
//	int arr[10] = {0,1,2,3,4,5,6,7,8,9,};
//	int left = 0;
//	int right = 0;
//	int mid = 0;
//	int k = 0;
//	int flag = 0;
//	right = sizeof(arr) / sizeof(arr[0]) - 1;
//	scanf("%d",&k);
//	while (left <= right)
//	{
//		mid = (left + right) / 2;
//		if (arr[mid] == k)
//		{
//			printf("找到了，下标为:%d", mid);
//			flag = 1;
//			break;
//		}
//		else if (arr[mid] < k)
//		{
//			left = mid + 1;
//		}
//		else
//		{
//			left = 0;
//			right = mid - 1;
//		}
//	}
//	if (flag == 0)
//	{
//		printf("找不到");
//	}
//	return 0;
//}






////					猜数字游戏
////			一、构建框架
////1.首先打印出来游戏界面；
////2.然后选择进行游戏或者退出游戏；
////3.在进行游戏内部调用game函数
////4.完成game函数的实现
////4.1.首先生成随机数
////4.2，进行判断；
//#include<stdio.h>
//#include<time.h>
//#include<stdlib.h>
//
//void game();
//void game()
//{
//	printf("请输入数字\n");
//	int s=rand()%100+1;
//	int input = 0;
//	while (1)
//	{
//		scanf("%d", &input);
//		if (s == input)
//		{
//			printf("猜对了!\n");
//			break;
//		}
//		if(s > input)
//		{
//			printf("小了，继续猜\n");
//		}
//		if (s < input)
//		{
//			printf("大了，继续猜\n");
//		}
//	}
//}
//int main()
//{
//	srand((unsigned int)time(NULL));
//	printf("*******************************\n");
//	printf("***1.开始游戏 **  0.退出游戏***\n");
//	printf("*******************************\n");
//	printf("*******************************\n");
//	int input = 0;
//	do
//	{
//		printf("请选择:\n");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏\n;");
//			break;
//		default:
//			printf("输入错误,重新选择\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}
//			


				/*实现函数判断year是不是润年。*/
//#include<stdio.h>
//void year(int y);
//void year(int y)
//{
//	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
//	{
//		printf("%d是闰年",y);
//	}
//	else
//	{
//		printf("%d不是闰年",y);
//	}
//}
//int main()
//{
//	int y = 0;
//	int years = 0;
//	scanf("%d", &years);
//	year(years);
//	return 0;
//}
//				


				//实现一个函数，判断一个数是不是素数。

				//利用上面实现的函数打印100到200之间的素数。
//#include<stdio.h>
//#include<math.h>
//int  judege(int n);
//int  judege(int n)
//{
//	int i = 0;
//	for (i = 2; i <= sqrt(n); i++)
//	{
//		if (n % i == 0)
//		{
//			return 0;
//		}
//	}
//	return 1;
//}
//int main()
//{
//	int n = 0;
//	for (n = 100; n <= 200; n++)
//	{
//		if (judege(n) == 1)
//		{
//			printf("%d\t", n);
//		}
//	}
//	return 0;
//}
#include<stdio.h>
#include<stdlib.h>
int main()
{
	int i = 0;
	while (i < 10)
	{
		int s = rand();
		printf("%d\t", s);
		i++;
	}
	
}