#define _CRT_SECURE_NO_WARNINGS 1
////#include<stdio.h>
////计算1 / 1 - 1 / 2 + 1 / 3 - 1 / 4 + 1 / 5 …… + 1 / 99 - 1 / 100 的值，打印出结果//
//int main()
//{ 
//	float x = 0;
//	float y = 0;
//	int i = 0;
//	float sum = 0;
//	for (i = 1; i <= 100; i+=2)
//	{
//		x = x + 1.0 / i;
//	}
//	for (i = 2; i <= 100; i +=2)
//	{
//		y = y + 1.0 / i;
//	}
//	sum = x - y;
//	printf("%lf", sum);
//	
//	return 0;
//}



//
//判验证switch语句中当没有break时，在最后一个语句停止
//#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//int func(int x);
//int func(int x)
//{
//	int b = 0;
//	switch(x)
//	{
//		default: b = 16;
//		case 1:b = 0;
//		case 2: b = 20;
//		case 3: b = 30;
//		
//	}
//	return b;
//}
//int main()
//{
//	int ret = 0;
//	ret = func(1);
//	printf("%d", ret);
//	return 0;
//	
//}

///////连加求和/////
//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	long long int sum = 0;
//	int n = 0;
//	scanf("%d", &n);
//	for (i = 0; i <= n; i++)
//	{
//		sum = sum + i;
//	}
//	printf("%lld", sum);
//	return 0;
//}




////数组中求最大值；
//#include <stdio.h>
//int main()
//{
//    int arr[4] = { 0 };
//    int i = 0;
//    int max = 0;
//    scanf("%d %d %d %d", &arr[0], &arr[1], &arr[2], &arr[3]);
//    max = arr[0];
//    for (i = 1; i < 4; i++)
//    {
//        if (arr[i] > max)
//        {
//            max = arr[i];
//
//        }
//    }
//    printf("%d", max);
//    return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int a = 20;
//	printf("%c", a);
//	return 0;
//
//}
//
////判断是否是字母
//#include <stdio.h>
//int main()
//{
//    char h = 0;
//    int s = 0;
//    while (scanf("%c", &h) != EOF)
//    {
//        s = h;
//        if (h != '\n')
//        {
//            if (((s >= 65) && (s <= 90)) || ((s >= 97) && (s <= 122)))
//            {
//                printf("%c is an alphabet.\n", h);
//                continue;
//            }
//            else
//            {
//                printf("%c is not an alphabet.\n", h);
//                continue;
//            }
//        }
//    }
//    return 0;
//}






//
//验证%c格式输入只能得到一个字符。
//#include<stdio.h>
//int main()
//{
//	char c = 0;
//	scanf("%c", &c);
//	printf("%c", c);
//	return 0;
//}




//KiKi非常喜欢网购，在一家店铺他看中了一件衣服，他了解到，如果今天是“双11”（11月11日）则这件衣服打7折，“双12”
//（12月12日）则这件衣服打8折，如果有优惠券可以额外减50元（优惠券只能在双11或双12使用），求KiKi最终所花的钱数。
#include <stdio.h>
int main()
{
    float v = 0.0;
    int m = 0;
    int d = 0;
    int q = 0;
    float zv = 0;
    scanf("%f %d %d %d", &v, &m, &d, &q);
    if ((m == 11 && d == 11))
    {
        if (q == 1)
        {
            zv = v * 0.7 - 50;
            if (zv <= 0)
            {
                zv = 0;
            }
        }
        else
        {
            zv = v * 0.7;
        }
    }
    else if ((m == 12 && d == 12))
    {
        if (q == 1)
        {
            zv = v * 0.8 - 50;
            if (zv <= 0)
            {
                zv = 0;
            }
        }
        else
        {
            zv = v * 0.8;
        }
    }
    else
    {
        zv = v;
    }
    printf("%.2f", zv);
    return 0;
}