#define _CRT_SECURE_NO_WARNINGS 1

//BC33 计算平均成绩
//#include<stdio.h>
//int main()
//{
//    int a, b, c, d, e;
//    scanf("%d%d%d%d%d", &a, &b, &c, &d, &e);
//    printf("%.1f", (a + b + c + d + e) / 5.0);
//    return 0;
//}


//BC34 进制A+B
//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    int b = 0;
//    scanf("%x%o", &a,&b);
//    printf("%d", a + b);
//    return 0;
//
//}

//BC35 判断字母


//#include<stdio.h>
//int main()
//{
//    char ch = 0;
//    scanf("%c", &ch);
//    if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
//    {
//        printf("YES");
//    }
//    else
//    {
//        printf("NO");
//    }
//    return 0;
//}

//BC36 健康评估



//#include<stdio.h>
//int main()
//{
//    int w = 0;
//    float m = 0.0f;
//    scanf("%d%f", &w, &m);
//    float bmi = 0.0f;
//    bmi = w / (m * m);
//    if (bmi >= 18.5 && bmi <= 23.9)
//    {
//        printf("Normal");
//    }
//    else
//    {
//        printf("Abnormal");
//    }
//    return 0;
//}

//BC37 网购


//#include<stdio.h>
//int main()
//{
//    float p = 0.0f;
//    int m = 0;
//    int d = 0;
//    int s = 0;
//    float sum = 0.0;
//    scanf("%f%d%d%d", &p, &m, &d, &s);
//    if (m == 11 && d == 11)
//    {
//        sum = 0.7 * p - (s * 50);
//    }
//    else if (m == 12 && d == 12)
//    {
//        sum = 0.8 * p - (s * 50);
//    }
//    else
//    {
//        sum = p - (s * 50);
//    }
//    if (sum <= 0)
//    {
//        sum = 0;
//    }
//    printf("%.2f", sum);
//    return 0;
//}



//BC38 变种水仙花
#include<stdio.h>
int main()
{
    int i = 0;
    for (i = 10000; i < 99999; i++)
    {
        int sum = 0;
        int j = 0;
        for (j = 10; j < 100000; j = j * 10)
        {
            sum += (i / j) * (i % j);
        }
        if (sum == i)
        {
            printf("%d ", i);
        }
    }
    return 0;
}

//BC39 争夺前五名

//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int i = 0;
//    scanf("%d", &n);
//    int arr[n];
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//
//    for (i = 0; i < n - 1; i++)
//    {
//        int j = 0;
//        for (j = 0; j < n - i - 1; j++)
//        {
//            if (arr[j] < arr[j + 1])
//            {
//                int tep = arr[j];
//                arr[j] = arr[j + 1];
//                arr[j + 1] = tep;
//            }
//        }
//    }
//    for (i = 0; i < 5; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}

////使用qsort函数进行解决！
//#include<stdio.h>
//#include<stdlib.h>
//int my_cmp(const void* a, const void* b)
//{
//    return (*(int*)b) - (*(int*)a);
//}
//
//int main()
//{
//    int n = 0;
//    int i = 0;
//    scanf("%d", &n);
//    int arr[n];
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    qsort(arr, n, sizeof(arr[0]), my_cmp);
//    for (i = 0; i < 5; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
