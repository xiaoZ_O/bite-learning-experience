
//反向输出一个数！
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
////递归方法！
//void print(int n)
//{
//    printf("%d",n%10);
//    if(n>=10)
//    {
//
//        print(n/10);
//    }
//}
//int main()
//{
//    int n=0;
//    scanf("%d",&n);
//    print(n);
//    return 0;
//}

//循环方法！
// 
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    while (n)
//    {
//        printf("%d", n % 10);
//        n /= 10;
//    }
//}

//kiki算数，100以内的数
//问题：KiKi今年5岁了，已经能够认识100以内的非负整数，并且并且能够进行 100 以内的非负整数的加法计算。不过，BoBo老师发现KiKi在进行大于等于100的正整数的计算时，规则如下：
//
//1.       只保留该数的最后两位，例如：对KiKi来说1234等价于34；
//
//2.       如果计算结果大于等于 100， 那么KIKI也仅保留计算结果的最后两位，如果此两位中十位为0，则只保留个位。
//
//例如：45 + 80 = 25
//
//要求给定非负整数 a和 b，模拟KiKi的运算规则计算出 a + b 的值。

//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    int b = 0;
//    int sum = 0;
//    scanf("%d%d", &a, &b);
//    sum = (a + b) % 100;
//    printf("%d", sum);
//    return 0;
//
//}

//给定一个浮点数，要求得到该浮点数的个位数。
//#include<stdio.h>
//int main(void)
//{
//    float f = 0.0f;
//    scanf("%f", &f);
//    int c = (int)f;
//    printf("%d", c % 10);
//    return 0;
//}


//问题：一年约有 3.156×107 s，要求输入您的年龄，显示该年龄合多少秒。
//数据范围：
//0≤200
//0 < age≤200
//#include<stdio.h>
//int main()
//{
//    long long age = 0;
//    scanf("%lld", &age);
//    long long s = 0;
//    s = 3.156e7 * age;
//    printf("%lld", s);
//    return 0;
//
//}

//时间转换！

//
//#include<stdio.h>
//int main()
//{
//    long int s = 0;
//    scanf("%ld", &s);
//    printf("%d %d %d", s / 3600, (s / 60) % 60, s % 60);
//    return 0;
//}

//BC24 总成绩和平均分计算

//#include<stdio.h>
//int main()
//{
//    float a = 0;
//    float b = 0;
//    float c = 0;
//    scanf("%f%f%f", &a, &b, &c);
//    printf("%.2f %.2f", (a + b + c), (a + b + c) / 3);
//}

//计算BMI指数!
//#include<stdio.h>
//int main()
//{
//    int w = 0;
//    int h = 0;
//    scanf("%d%d", &w, &h);
//    printf("%.2f", w / ((h / 100.0) * (h / 100.0)));
//    return 0;
//}


//BC26 计算三角形的周长和面积
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//    int a, b, c;
//    scanf("%d%d%d", &a, &b, &c);
//    float s = a + b + c;
//    float p = s / 2.0;
//    float z = sqrt(p * (p - a) * (p - b) * (p - c));
//    printf("circumference=%.2f area=%.2f", s, z);
//    return 0;
//}
// 
// 
//计算球的体积！
// 
// 
//#define pai 3.1415926
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//    double n = 0.0f;
//    scanf("%lf", &n);
//    double v = 0.0f;
//    v = (4.0 / 3.0) * pai * pow(n, 3.0);
//    printf("%.3lf", v);
//    return 0;
//
//}
//将大写字母转化为小写！  仅需将大写字母的ASCII码值+32即可！


//#include<stdio.h>
//int main()
//{
//    char a = 0;
//    while (~scanf("\n%c", &a))
//    {
//        printf("%c\n", a + 32);
//    }
//    return 0;
//}

//2的n次方计算
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int a = (1 << n);
//    printf("%d", a);
//    return 0;
//
//}



//BoBo买了一箱酸奶，里面有n盒未打开的酸奶，KiKi喜欢喝酸奶，第一时间发现了酸奶。KiKi每h分钟能喝光一盒酸奶，
//并且KiKi在喝光一盒酸奶之前不会喝另一个，那么经过m分钟后还有多少盒未打开的酸奶？

//#include<stdio.h>
//int main()
//{
//    int a, b, c;
//    scanf("%d%d%d", &a, &b, &c);
//    if (c % b)
//    {
//        a = a - (c / b) - 1;
//    }
//    else {
//        a = a - c / b;
//    }
//    printf("%d", a);
//    return 0;
//}