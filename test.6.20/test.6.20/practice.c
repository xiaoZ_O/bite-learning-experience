#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//void test(arr)//ok?
//{
//
//}
//int main()
//{
//	int arr[10] = { 0 };
//	int* arr2[20] = { 0 };
//	test(arr);
//}

//计算机的实现！  普通方法
void Menu()
{
	printf("***********************\n");
	printf("******1.Add  2.Sub*****\n");
	printf("******3.Mul  4.Div*****\n");
	printf("*******  0.Exit    ****\n");
	printf("***********************\n");
}
int Add(int x, int y)
{
	return x + y;
}

int Sub(int x, int y)
{
	return x - y;
}

int Mul(int x, int y)
{
	return x * y;
}

int Div(int x, int y)
{
	return x / y;
}
//int main()
//{
//	int input = 0;
//	do
//	{
//		int x = 0;
//		int y = 0;
//		Menu();
//		printf("请进行选择->");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 0:break;
//		case 1:
//			printf("请输入两个操作数：");
//			scanf("%d%d", &x, &y);
//			int ret = Add(x, y);
//			printf("%d\n", ret);
//			break;
//		case 2:
//			printf("请输入两个操作数：");
//			scanf("%d%d", &x, &y);
//			ret = Sub(x, y);
//			printf("%d\n", ret);
//			break;
//		case 3:
//			printf("请输入两个操作数：");
//			scanf("%d%d", &x, &y);
//			ret = Mul(x, y);
//			printf("%d\n", ret);
//			break;
//		case 4:
//			printf("请输入两个操作数：");
//			scanf("%d%d", &x, &y);
//			ret = Div(x, y);
//			printf("%d\n", ret);
//			break;
//		default :
//			printf("输入错误，请重新输入\n");
//			break;
//		}
//	} while (input);
//}


//利用函数指针数组实现计算机！

int main()
{
	int input = 1;
	int x = 0;
	int y = 0;
	int ret = 0;
	while(input)
	{
		Menu();
		printf("请进行选择->");
		scanf("%d", &input);
		int(*p[5])(int x,int y) = {0,Add,Sub,Mul,Div};										//函数指针数组
		//int(*p[5])(int x, int y) = { 0, add, sub, mul, div };
		if ((input <= 4 && input >= 1))
		{
			printf("输入操作数：");
			scanf("%d %d", &x, &y);
			ret = (p[input])(x, y);
			printf("ret = %d\n", ret);
		}
		else
		{
			printf("输入错误,重新选择！\n");
		}
		
	}
}



















