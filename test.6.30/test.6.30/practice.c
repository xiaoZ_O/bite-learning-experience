#define _CRT_SECURE_NO_WARNINGS 1
//牛客计数器的实现   if语句完成
#include<stdio.h>
//int main()
//{
//    double a = 0;
//    double c = 0;
//    char b = '0';
//    scanf("\n%lf%c%lf", &a, &b, &c);
//    if (b == '+' || b == '-' || b == '*' || b == '/')
//    {
//        if (b == '/' && c == 0.0f)
//        {
//            printf("Wrong!Division by zero!\n");
//        }
//        else if (b == '+')
//        {
//            printf("%.4lf+%.4lf=%.4lf", a, c, a + c);
//        }
//        else if (b == '-')
//        {
//            printf("%.4lf-%.4lf=%.4lf", a, c, a - c);
//        }
//        else if (b == '*')
//        {
//            printf("%.4lf*%.4lf=%.4lf", a, c, a * c);
//        }
//        else if (b == '/')
//        {
//            printf("%.4lf/%.4lf=%.4lf", a, c, a / c);
//        }
//
//    }
//    else
//    {
//        printf("Invalid operation!\n");
//    }
//    return 0;
//}

//swith 语句实现！
#include<stdio.h>
//int main()
//{
//    double a = 0.0f;
//    double b = 0.0f;
//    char c = '0';
//    scanf("%lf%c%lf", &a, &c, &b);
//    switch (c)
//    {
//    case '+':
//        printf("%.4lf+%.4lf=%.4lf", a, b, a + b);
//        break;
//    case '-':
//        printf("%.4lf-%.4lf=%.4lf", a, b, a - b);
//        break;
//    case '*':
//        printf("%.4lf*%.4lf=%.4lf", a, b, a * b);
//        break;
//    case '/':
//        if (b == 0)
//        {
//            printf("Wrong!Division by zero!\n");
//            break;
//        }
//        else
//        {
//            printf("%.4lf/%.4lf=%.4lf", a, b, a / b);
//            break;
//        }
//
//    default:
//        printf("Invalid operation!\n");
//        break;
//    }
//    return 0;
//}

//BC56 线段图案
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        for (int i = 0; i < n; i++)
//        {
//            printf("*");
//        }
//        printf("\n");
//    }
//    return 0;
//}
//BC57 正方形图案
#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}
//


//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (j <= i)
//                {
//                    printf("* ");
//                }
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}


//BC59 翻转直角三角形图案
//#include <stdio.h>
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (i <= j)
//                {
//                    printf("* ");
//                }
//            }
//            printf("\n");
//        }
//    }
//}

//BC60 带空格直角三角形图案
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (i + j == n - 1 || i + j >= n)
//                {
//                    printf("* ");
//                }
//                else
//                {
//                    printf("  ");
//                }
//            }
//            printf("\n");
//        }
//    }
//}


//BC61 金字塔图案
#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int i = 0;
//    int j = 0;
//    while (~scanf("%d", &n))
//    {
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j < n - i - 1; j++) //控制打印空格
//            {
//                printf(" ");
//            }
//            for (int k = 0; k <= i; k++)    //控制打印* ;
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//}
//BC62 翻转金字塔图案
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int i = 0;
//    int j = 0;
//    while (~scanf("%d", &n))
//    {
//        for (i = 0; i < n; i++)
//        {
//            //控制打印空格
//            for (j = 0; j < i; j++)
//            {
//                printf(" ");
//            }
//            for (int k = 0; k <= n - i - 1; k++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//
//    }
//    return 0;
//}

//BC63 菱形图案
#include<stdio.h>
int main() {
    int n = 0;
    int i = 0;
    int k = 0;
    int h = 0;
    while (~scanf("%d", &n))
    {
        //上半部分打印
        //首先打印空格，其次再打印*
        for (int h = 0; h <= n; h++) //控制上半·部分的行数！
        {
            for (i = 0; i < n - h; i++)
            {
                printf(" ");
            }
            for (k = 0; k <= h; k++)
            {
                printf("* ");
            }
            printf("\n");
        }
        //其次打印下半部分！下半部分同样也是先打印空格，然后打印*
        //需要主要的是下半部分正好与上半部分相反！
        for (h = 0; h < n; h++)
        {
            for (i = 0; i <= h; i++)
            {
                printf(" ");
            }
            for (k = 0; k < n - h; k++)
            {
                printf("* ");
            }
            printf("\n");
        }
    }
}