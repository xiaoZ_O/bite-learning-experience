﻿#define _CRT_SECURE_NO_WARNINGS 1
//BC85 包含数字9的数   
//采用模10除10的方法求出每一位数字，观察是否符合要求！
//#include<stdio.h> 
//int main()
//{
//    int i = 0;
//    int count = 0;
//    for (i = 1; i <= 2019; i++)
//    {
//        int t = i;
//        while (t >= 9)
//        {
//            if (t % 10 == 9)
//            {
//                count++;
//                break;
//            }
//            t /= 10;
//        }
//    }
//    printf("%d", count);
//}

//BC86 奇偶统计
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int add = 0;
//    int odd = 0;
//    int i = 0;
//    scanf("%d", &n);
//    for (i = 1; i <= n; i++)
//    {
//        int t = i;
//        if (t % 2 == 0)
//        {
//            odd++;
//        }
//        else {
//            add++;
//        }
//    }
//    printf("%d %d", add, odd);
//}



//BC87 统计成绩
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
//int cmp(void* e1, void* e2)
//{
//    return *(double*)e1 - *(double*)e2; //升序，最后一个为最大值！
//}
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    double arr[n];
//    float sum = 0.0f;
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%lf", &arr[i]);
//        sum += arr[i];
//    }
//    qsort(arr, n, sizeof(arr[0]), cmp);
//    printf("%.2lf %.2f %.2f", arr[n - 1], arr[0], sum / 5.0);
//    return 0;
//}




//#include<stdio.h>
//#include<string.h>
//#include<stdlib.h>
//int cmp(void* e1, void* e2)
//{
//    return *(double*)e1 - *(double*)e2; //升序，最后一个为最大值！
//}
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    double a, b, c, d, e;
//    scanf("%lf%lf%lf%lf%lf", &a, &b, &c, &d, &e);
//    double arr[100] = { a,b,c,d,e };
//    float sum = a + b + c + d + e;
//    qsort(arr, n, sizeof(arr[0]), cmp);
//    printf("%.2lf %.2lf %.2f", arr[n - 1], arr[0], sum / 5.0);
//    return 0;
//}





//BC88 有序序列插入一个数

//利用qsort函数实现
//#include<stdio.h>
//#include<stdlib.h>
//int cmp(void* e1, void* e2)
//{
//    return *(int*)e1 - *(int*)e2;
//}
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int arr[n + 1];
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    scanf("%d", &arr[n]);
//    qsort(arr, n + 1, sizeof(arr[0]), cmp);
//    for (int i = 0; i < n + 1; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//}

//利用冒泡排序法进行实现！
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int arr[n + 1];
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    scanf("%d", &arr[n]);
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < n - i; j++)
//        {
//            if (arr[j] > arr[j + 1])
//            {
//                int tem = arr[j];
//                arr[j] = arr[j + 1];
//                arr[j + 1] = tem;
//            }
//        }
//    }
//    for (int i = 0; i < n + 1; i++)
//
//    {
//        printf("%d ", arr[i]);
//    }
//}



//BC89 密码验证
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//    char arr1[100] = { 0 };
//    char arr2[100] = { 0 };
//    scanf("%s%s", arr1, arr2);
//    if (strcmp(arr1, arr2) == 0)
//    {
//        printf("same");
//    }
//    else {
//        printf("different");
//    }
//}



//#include <stdio.h>
//int main()
//{
//	char a = -1;
//	signed char b = -1;
//	unsigned char c = -1;
//	printf("a=%d,b=%d,c=%d", a, b, c);   //-1 -1  255
//	return 0;
//}

//
//#include <stdio.h>
//int main()
//{
//	char a = -128;
//	//1 10000000
//	printf("%u\n", a);//4294967168
//	return 0;
//}

#include <stdio.h>
//int main()
//{
//	char a = 128;
//	printf("%u\n", a);//11111111111111111111111110000000
//	return 0;
//}

//int main()
//{
//	int i = -20;
//	unsigned int j = 10;
//	printf("%d\n", i + j);
//}

//
////#include<float.h>
//#include<stdio.h>
//int main()
//{
//	int n = 9;
//	float* pFloat = (float*)&n;
//	printf("n的值为：%d\n", n);			
//	printf("*pFloat的值为：%f\n", *pFloat);
//	*pFloat = 9.0;
//	printf("num的值为：%d\n", n);
//	printf("*pFloat的值为：%f\n", *pFloat);
//	return 0;
//}

//int main()
//{
//	int i = -1;
//	printf("%u", i);
//}

#include <stdio.h>
void print(unsigned int n)
{
	if (n > 9)
	{
		print(n / 10);
	}
	//else if (n < -9)
	//{
	//	print(n / 10);
	//}
	printf("%u ", n % 10);
}
int main()
{
	int num = 0;
	scanf("%d", &num);
	print(num);
	return 0;
}