#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//先实现冒泡排序的代码！
//void bubble_sort(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz-1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tem = 0;
//				tem = arr[j];
//				arr[j] = arr[j+1];
//				arr[j + 1] = tem;
//			}
//		}
//	}
//}
void print(int arr[], int num)
{
	int i = 0;
	for (i = 0; i < num; i++)
	{
		printf("%d ", arr[i] );
	}
}
//int main()
//{
//	int arr[10] = { 0,1,3,5,7,9,2,4,6,8 };
//	int sz = sizeof(arr) /sizeof(arr[0]);
//	bubble_sort(arr, sz);
//	print(arr, sz);
//	return 0;
//}


//使用bubble_sort 实现qsort！





int int_cmp( void* e1,  void* e2)
{
	return (*(int *)e1 - *(int *)e2);
}
void Swap( void* e1, void* e2,int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
	{
		char tem = *((char *)e1+i);
		*((char *)e1+i) = *((char *)e2+i);
		*((char *)e2+i) = tem;
	}
}

void bubble_sort(void*base,
				int  num,
				int size,
	int (*cmp)(void *,void *)
				)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < num - 1; i++)
	{

		for (j = 0; j < num - 1 - i; j++)
		{
			if (cmp((char*)base + j * size, (char*)base + (j + 1) * size))
			{
				Swap((char*)base + j * size, (char*)base + (j + 1)*size, size);
			}
		}
	}
}
int main()
{
	int arr[] = { 0,1,3,5,7,9,2,4,6,8 };
	int num = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr,num,sizeof(arr[0]), int_cmp);
	print(arr, num);
	return 0;
}