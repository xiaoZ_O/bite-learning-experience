#define _CRT_SECURE_NO_WARNINGS 1
//BC64 K形图案
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int i = 0;
//    int j = 0;
//    while (~scanf("%d", &n))
//    {
//        //做法与菱形的打印相同，将其分为两大部分即可!
//        //上半部分的打印如下：
//        for (i = 0; i <= n; i++)
//        {
//            for (j = 0; j <= n - i; j++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//        //下半部分的打印如下:
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j <= i + 1; j++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}



//BC65 箭形图案
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int i = 0;
//    int j = 0;
//    while (~scanf("%d", &n))
//    {
//        //分两部分打印！
//        //首先打印飞机上半面！
//        for (i = 0; i <= n; i++)
//        {
//            //上半面又分为空格和*的打印！
//            //首先打印空格！
//            for (j = 0; j < 2 * (n - i); j++)
//            {
//                printf(" ");
//            }
//            for (j = 0; j <= i; j++)
//            {
//                printf("*");
//            }
//            printf("\n");
//        }
//        //下半部分的打印！
//        //下半部分的行数控制！
//        for (i = 0; i < n; i++)
//        {
//            //空格分别为2*(i+1);
//            for (j = 0; j < 2 * (i + 1); j++)
//            {
//                printf(" ");
//            }
//            for (j = 0; j < n - i; j++)
//            {
//                printf("*");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}



//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int i = 0;
//    int j = 0;
//    while (~scanf("%d", &n))
//    {
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j < n; j++)
//            {
//                if (i == j)
//                {
//                    printf("*");
//                }
//                else
//                {
//                    printf(" ");
//                }
//            }
//            printf("\n");
//        }
//        
//    }
//    return 0;
//}



//BC67 正斜线形图案
//两坐标相加等于n-1即为对应位置！
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        int i = 0;
//        int j = 0;
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j < n; j++)
//            {
//                if (i + j == n - 1)
//                {
//                    printf("*");
//                }
//                else
//                {
//                    printf(" ");
//                }
//
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}


//BC68 X形图案
//#include<stdio.h>
//int main()
//{
//    //根据图像要求可以得出符合*坐标的位置
//    //1.  i+j==n-1
//    //2.  j==i
//    int n = 0;
//    int i = 0;
//    int j = 0;
//    while (~scanf("%d", &n))
//    {
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j < n; j++)
//            {
//                if ((i + j == n - 1) || i == j)
//                    printf("*");
//                else
//                {
//                    printf(" ");
//                };
//            }
//            printf("\n");
//        }
//    }
//}

//BC69 空心正方形图案
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        int i = 0;
//        int j = 0;
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j < n; j++)
//            {
//                if (i == 0 || i == n - 1 || j == 0 || j == n - 1)
//                {
//                    printf("* ");
//                }
//                else
//                {
//                    printf("  ");
//                }
//            }
//            printf("\n");
//        }
//    }
//}

//BC70 空心三角形图案
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        int i = 0;
//        int j = 0;
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j < n; j++)
//            {
//                if (j == 0 || i == n - 1 || i == j)
//                {
//                    printf("* ");
//                }
//                else
//                {
//                    printf("  ");
//                }
//            }
//            printf("\n");
//        }
//    }
//}