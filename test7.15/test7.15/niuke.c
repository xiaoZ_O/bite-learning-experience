#define _CRT_SECURE_NO_WARNINGS 1
//BC99 序列中整数去重
// 
//  法一！
//#include<stdio.h>
//int main()
//{
//    int arr1[50] = { 0 };
//    int arr2[50] = { 0 };
//    int n = 0;  //统计数组1中元素的个数！
//    int q = 0;  //用来控制数组2的下标！
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//    {
//        int flag = 0;  //用来标记是否有相同的元素！有则返回1！
//        scanf("%d", &arr1[i]);
//        for (int j = 0; j < i; j++)
//        {
//            if (arr2[j] == arr1[i])
//            {
//                flag = 1;
//                break;
//            }
//        }
//        if (flag == 0)
//        {
//            arr2[q] = arr1[i];
//            q++;
//        }
//    }
//    for (int i = 0; i < q; i++)
//    {
//        printf("%d ", arr2[i]);
//    }
//    return 0;
//}
                    //法2！（重点！）

//#include <stdio.h>
//int main()
//{
//    int n = 0;
//    int arr[1000] = { 0 };
//    //输入
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    //去重
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = i + 1; j < n; j++)
//        {
//            if (arr[i] == arr[j])
//            {
//                //删除
//                for (int k = j; k < n - 1; k++)
//                {
//                    arr[k] = arr[k + 1];
//                }
//                n--;
//                j--;
//            }
//        }
//    }
//    //输出
//    for (int i = 0; i < n; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//
//    return 0;
//}


        //BC100 有序序列合并
            //！法1
//int main()
//{
//    int arr1[2000];
//    int arr2[2000];
//    int i = 0;
//    int m = 0;
//    int n = 0;
//    int j = 0;
//    scanf("%d%d", &m, &n);
//    for (i = 0; i < m; i++)
//    {
//        scanf("%d", &arr1[i]);
//    }
//    for (j = 0; j < n; j++)
//    {
//        scanf("%d", &arr2[j]);
//    }
//    i = 0;
//    j = 0;
//    while (i < m && j < n)
//    {
//
//        if (arr1[i] < arr2[j])
//        {
//            printf("%d ", arr1[i]);
//            i++;
//        }
//        else
//        {
//            printf("%d ", arr2[j]);
//            j++;
//        }
//    }
//    while (i < m || j < n)
//    {
//        if (j < n && i == m)
//        {
//            printf("%d ", arr2[j]);
//            j++;
//        }
//        else
//        {
//            printf("%d ", arr1[i]);
//            i++;
//        }
//    }
//    return 0;
//}
            //使用qsort函数进行求解！
//#include<stdio.h>
//#include<string.h>
//#include<stdlib.h>
//int cmp(void* e1, void* e2)
//{
//    return *(int*)e1 - *(int*)e2;
//}
//int main()
//{
//    int n, m, i = 0;
//    scanf("%d%d", &n, &m);
//    int arr[m + n];
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    for (i = 0; i < m; i++)
//    {
//        scanf("%d", &arr[n + i]);
//    }
//    qsort(arr, n + m, sizeof(int), cmp);
//    for (i = 0; i < n + m; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}


//BC101 班级成绩输入输出

//#include<stdio.h>
//int main()
//{
//
//    float grades[5][5] = { 0 };
//    for (int i = 0; i < 5; i++)
//    {
//        for (int j = 0; j < 5; j++)
//        {
//            scanf("%f", &grades[i][j]);
//
//        }
//    }
//    for (int i = 0; i < 5; i++)
//    {
//        float sum = 0.0f;
//        for (int j = 0; j < 5; j++)
//        {
//            sum += grades[i][j];
//            printf("%.1f ", grades[i][j]);
//        }
//        printf("%.1f", sum);
//        printf("\n");
//    }
//    return 0;
//}

//BC102 矩阵元素定位

//#include<stdio.h>
//int main()
//{
//    int row = 0;
//    int col = 0;
//    scanf("%d%d", &row, &col);
//    int arr[row][col];
//    for (int i = 1; i <= row; i++)
//    {
//        for (int j = 1; j <= col; j++)
//        {
//            scanf("%d", &arr[i - 1][j - 1]);
//        }
//    }
//    int x, y = 0;
//    scanf("%d %d", &x, &y);
//    printf("%d", arr[x - 1][y - 1]);
//}