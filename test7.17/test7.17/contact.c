#define _CRT_SECURE_NO_WARNINGS 1
#include"contact.h"

//初始化通讯录!
void InitContact(Contact* ptr)
{
	ptr->sz = 0;
	memset(ptr->data, 0, sizeof(ptr->data));
}

//增加成员的功能！
void Addcontact(Contact* ptr)
{
	if (ptr->sz == MAX)
	{
		printf("通讯录已满！\n");
		return;
	}
	else
	{
		printf("请输入姓名->");
		scanf("%s", ptr->data[ptr->sz].name);
		printf("请输入年龄->");
		scanf("%d", &(ptr->data[ptr->sz].age));
		printf("请输入性别->");
		scanf("%s", ptr->data[ptr->sz].sex);
		(ptr->sz)++;
		printf("增加成功！\n");
		return;
	}
}

//打印现有的通讯录
void Showcontact(Contact* ptr)
{
	int i = 0;
	printf("%-5s %-5s %-15s\n", "姓名", "年龄", "性别");
	for (i = 0; i < ptr->sz; i++)
	{
		printf("%-5s %-5d %-15s\n", ptr->data[i].name, ptr->data[i].age, ptr->data[i].sex);
	}
}


//寻找通讯录中的成员!
int Findcontact(Contact* ptr,char name[])
{
	int pos = 0;
	int i = 0;
	for (i = 0; i < ptr->sz; i++)
	{
		if (strcmp(ptr->data[i].name, name) == 0)
		{
			pos = i;
			return pos;
		}
	}
	return -1;
}

//删除通讯录中的成员！
void Delcontact(Contact* ptr)
{
	char name[MAX_NAME];
	printf("请输入要删除的成员名字\n");
	scanf("%s", name);
	int pos=Findcontact(ptr,name);
	if (pos == -1)
	{
		printf("不存在该成员！\n");
		return;

	}
	for (int i = pos; pos < ptr->sz-1; i++)
	{
		ptr->data[i] = ptr->data[i + 1];

	}
	ptr->sz--;
	printf("删除成功！\n");
}
//查找并打印!
void Seacontact(Contact* ptr)
{
	char name[MAX_NAME]={0};
	printf("请输入你要查找的成员");
	scanf("%s", name);
	int pos = Findcontact(ptr, name);
	if (pos == -1)
	{
		printf("不存在此成员\n");
		return;
	}
	printf("%-5s %-5s %-15s\n","姓名", "年龄", "性别");
	printf("%-5s %-5d %-15s",
		ptr->data[pos].name,
		ptr->data[pos].age,
		ptr->data[pos].sex);
	return;
}

//修改通讯录中成员的信息！
void Modcontact(Contact* ptr)
{
	char name[MAX_NAME];
	printf("请输入你要修改的成员！");
	scanf("%s", name);
	int pos = Findcontact(ptr, name);
	if (pos == -1)
	{
		printf("不存在此成员！\n");
		return;
	}
	printf("请输入修改后的姓名");
	scanf("%s", ptr->data[pos].name);
	printf("修改后的年龄");
	scanf("%d", &(ptr->data[pos].age));
	printf("修改后的性别");
	scanf("%s", ptr->data[pos].sex);
	printf("修改成功\n");
}

int cmp_byname(const void* e1, const void* e2)
{
	return strcmp(((Peo*)e1)->name, ((Peo*)e2)->name);
}
//用名字来实现排序！
void Sortcontact(Contact* ptr)
{
	qsort(ptr->data, ptr->sz, sizeof(Peo), cmp_byname);
	printf("排序成功！\n");
}






//初始化通讯录
void Initcontact(Contact* ptr)
{
	memset(ptr->data, 0, sizeof(ptr->data));
	ptr->sz = 0;
}