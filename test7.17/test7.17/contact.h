#define _CRT_SECURE_NO_WARNINGS  1 
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define MAX 100
#define MAX_NAME 20
#define MAX_SEX 5
#define MAX_ADDR 30
#define MAX_NUM 13


typedef struct peoinfo
{
	char name[MAX_NAME];
	int age;
	char addr[MAX_ADDR];
	char sex[MAX_SEX];
	int num[MAX_NUM];
}Peo;

typedef struct contact
{
	Peo data[MAX];
	int sz;
}Contact;

//实现通讯录的增加好友信息功能！
void Addcontact(Contact* ptr);
//实现查找通讯员中的某一成员的位置！
int Findcontact(Contact* ptr, char name[]);
//删除通讯录中的成员！
void Delcontact(Contact* ptr);
//查找通讯录中的某一成员，并对其信息进行打印！
void Seacontact(Contact* ptr);
//修改通讯录成员的信息！
void Modcontact(Contact* ptr);
//用名字来排序
int cmp_byname(const void* e1, const void* e2);
void Sortcontact(Contact* ptr);

//打印现存的通讯录名单！
void Showcontact(Contact* ptr);

//初始化通讯录！
void Initcontact(Contact* ptr);