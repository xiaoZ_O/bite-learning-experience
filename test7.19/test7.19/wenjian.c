#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<errno.h>
//从文件中读取字符，练习使用fgetc函数，切记该函数返回的是一个int类型的值！每读取一个字符，指针自动往下移动一下！
// 因为可能会存在EOF所以采用了int来接收，char类型不能接收EOF！
//int main()
//{
//	FILE* pf;
//	pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//	}
//	else
//	{
//		char a=fgetc(pf);
//		char b=fgetc(pf);
//		char c=fgetc(pf);
//		char d=fgetc(pf);
//		printf("%c%c%c%c", a,b,c,d);
//	}
//	fclose(pf);
//	pf = NULL;
//}

//fputc函数的作用是在文件中输入一个字符，并且会将原有的字符全部删除，只留下当前输入的字符！
//若文件不存在，用w只写的方式打开的情况下会自动创建一个新的文件1
int main()
{
	FILE* pf;
	pf = fopen("test.txt", "w");
	if (pf == NULL)
	{
		perror("fopen");
	}
	else
	{
		int i = 0;
		for (i = 0; i < 26; i++)
		{
			char ch = 0;
			ch=fputc('a' + i, pf);
			printf("%c ", ch);
		}

	}
	fclose(pf);
	pf = NULL;
}



//fputs 函数的练习使用！  //利用了绝对路径，注意在使用绝对路径的情况下也得记得加上其文件名，要不还是实现不了对文件的打开
//并且练习使用了以"a"追加的方式打开文件，发现了其余w打开方式的区别不同！
//"a"追加并不会对原文件内容做修改，而"w"则会对原文件先清空再进行追加！
//int main()
//{
//	FILE* pf;
//	pf = fopen("C:\\Users\\asus\\Desktop\\test2.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//	}
//
//	else
//	{
//		fputs("zhou123", pf);
//	}
//	fclose(pf);
//	pf = NULL;
//}



//fgets函数读取一行数据！   其中第一个参数表示存放后面读取元素的数组，用来存放以后读出来的元素！第二个参数表示读取元素的个数！
//int main()
//{
//	FILE* pf;
//	pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//			perror("fopen");
//	}
//	
//	else
//	{
//		char arr[20];
//		char*ch=fgets(arr,5,pf);
//		puts(ch);
//	}
//	fclose(pf);
//	pf = NULL;
//}



//练习使用格式化输入函数，
//sprintf格式化输出函数作用就是将格式化的输出转化为一个字符串输出！：例如  sscanf; （从内存中写出数据到文件中！）
//sscanf函数的作用就是将将一个字符串的输入转化为格式化的输入!(写入数据到内存中)
//struct stu
//{
//	int age;
//	char name[20];
//	char sex[3];
//}s1={15,"zhangsan","nan"};
//
//int main()
//{
//	FILE* pf;
//	pf = fopen("test.txt", "w");
//	if (pf == NULL)
//	{
//		perror("errno");
//		return 1;
//	}
//	else
//	{
//		char arr[200] = { 0 };
//		sprintf(arr,"%d %s %s\n", s1.age, s1.name, s1.sex);
//		struct stu tem = { 0 };
//		printf("字符串形式打印：%s", arr);
//		sscanf(arr,"%d %s %s\n", &(tem.age), tem.name, tem.sex);
//		printf("格式化形式打印：%d %s %s\n", tem.age, tem.name, tem.sex);
//		fclose(pf);
//		pf = NULL;
//	}
//	return 0;
//}





//fprint函数的使用，写文件!
//struct stu
//{
//	int age;
//	char name[20];
//	char sex[5];
//}s1={15,"wangwu","nan"};
//int main()
//{
//	FILE* fp = fopen("test.txt", "w");
//	if (fp == NULL)
//	{
//		perror("errno");
//		return 0;
//	}
//	fprintf(fp, "%d %s %s", s1.age, s1.name, s1.sex);
//	fclose(fp);
//	fp = NULL;
//	return 1;
//}


//fscanf函数读取文件！
//struct stu
//{
//	int age;
//	char name[20];
//	char sex[5];
//}s1;
//int main()
//{
//	FILE* fp = fopen("test.txt", "r");
//	if (fp == NULL)
//	{
//		perror("errno");
//		return 0;
//	}
//	struct stu tem = { 0 };
//	fscanf(fp, "%d %s %s", &(tem.age), tem.name, tem.sex);
//	printf("%d %s %s", tem.age, tem.name, tem.sex);
//	fclose(fp);
//	fp = NULL;
//	return 1;
//}