#define _CRT_SECURE_NO_WARNINGS 1

//BC71 新年快乐
//#include<stdio.h>
//int main()
//{
//    printf("Happy New Year*2019*");
//}


//BC72 平均身高
//#include<stdio.h>
//int main()
//{
//    double a, b, c, d, e;
//    scanf("%lf%lf%lf%lf%lf", &a, &b, &c, &d, &e);
//    printf("%.2lf", (a + b + c + d + e) / 5.0);
//    return 0;
//}

//BC73 挂科危险
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    if (n >= 10)
//    {
//        printf("Danger++\n");
//    }
//    else if (n >= 4 && n <= 9)
//    {
//        printf("Danger\n");
//    }
//    else {
//        printf("Good\n");
//    }
//    return 0;
//}
// 
// 
// 
//BC74 HTTP状态码
//#include<stdio.h>
//int main()
//{
//    int input = 0;
//    while (~scanf("%d", &input))
//    {
//        switch (input)
//        {
//        case 200:
//            printf("OK\n");
//            break;
//
//        case 202:
//            printf("Accepted\n");
//            break;
//
//        case 400:
//            printf("Bad Request\n");
//            break;
//
//        case 403:
//            printf("Forbidden\n");
//            break;
//
//        case 404:
//            printf("Not Found\n");
//            break;
//
//        case 500:
//            printf("Internal Server Error\n");
//            break;
//
//        case 502:
//            printf("Bad Gateway\n");
//            break;
//        default:
//            break;
//        }
//
//    }
//    return 0;
//}


//BC75 数字三角形
//#include<stdio.h>
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        int i = 0;
//        int j = 0;
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j <= i; j++)
//            {
//                printf("%d ", j + 1);
//            }
//            printf("\n");
//        }
//    }
//}

//BC76 公务员面试
#include <stdio.h>
//此种方法只能进行一行的输入，若想实现多组输入，采取每次while(~scanf(%d%d%d,&a[0],&b[1],&c[2]))即可
//void bubble_sort(int arr[], int sz)
//{
//    int i = 0;
//    int j = 0;
//    for (i = 0; i < sz - 1; i++)
//    {
//        for (j = 0; j < sz - 1 - i; j++)
//        {
//            if (arr[j] < arr[j + 1])
//            {
//                int tem = arr[j];
//                arr[j] = arr[j + 1];
//                arr[j + 1] = tem;
//            }
//        }
//    }
//}
//int main()
//{
//    int arr[7] = { 0 };
//    int i = 0;
//    int sz = 7;
//    int sum = 0;
//    double ave = 0.0f;
//    for (i = 0; i < 7; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    bubble_sort(arr, sz);
//    for (i = 1; i < sz - 1; i++)
//    {
//        sum += arr[i];
//    }
//    printf("%.2lf", sum / 5.0);
//}

//BC77 有序序列插入一个数

//#include<stdio.h>
//void bubble(int arr[], int sz)
//{
//    for (int i = 0; i < sz - 1; i++)
//    {
//        for (int j = 0; j < sz - 1 - i; j++)
//        {
//            if (arr[j] > arr[j + 1])
//            {
//                int tem = arr[j];
//                arr[j] = arr[j + 1];
//                arr[j + 1] = tem;
//            }
//        }
//    }
//}
//int main()
//{
//    int i = 0;
//    int n = 0;
//    scanf("%d", &n);
//    int arr[n + 1];
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    arr[n] = 0;
//    scanf("%d", &arr[n]);
//    bubble(arr, n + 1);
//    for (i = 0; i < n + 1; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}

//BC78 筛选法求素数
//列举未涉及数组
//#include <stdio.h>
//int main()
//{
//    int n = 0;
//    int count = 0;
//    while (~scanf("%d", &n))
//    {
//        for (int i = 2; i <= n; i++)
//        {
//            int j = 0;
//            for (j = 2; j <= i; j++)
//            {
//                if (i % j == 0)
//                {
//                    break;
//                }
//            }
//            if (i == j)
//            {
//                printf("%d ", i);
//                count++;
//            }
//        }
//        printf("\n%d", n - 2 + 1 - count);
//    }
//    return 0;
//}
//数组求解
//int main()
//{
//int n = 0;
//int i = 0;
//while (~scanf("%d", &n))
//{
//    int j = 2;
//    int count = 0;
//    int arr[n];
//    for (i = 2; i <= n; i++)
//    {
//        for (j = 2; j < i; j++)
//        {
//            if (i % j == 0)
//            {
//                arr[i - 2] = 0;
//                count++;
//                break;
//            }
//        }
//        if (i == j)
//        {
//            printf("%d ", i);
//        }
//    }
//    printf("\n%d", count);
//}
//return 0;
//}

//BC79 图像相似度
//#include<stdio.h>
//int main()
//{
//    int h = 0;
//    int l = 0;
//    int count = 0;
//    scanf("%d%d", &h, &l);
//    int arr1[h][l];
//    int arr2[h][l];
//    for (int i = 0; i < h; i++)
//    {
//        for (int j = 0; j < l; j++)
//        {
//            scanf("%d", &arr1[i][j]);
//        }
//    }
//    for (int i = 0; i < h; i++)
//    {
//        for (int j = 0; j < l; j++)
//        {
//            scanf("%d", &arr2[i][j]);
//        }
//    }
//    for (int i = 0; i < h; i++)
//    {
//        for (int j = 0; j < l; j++)
//        {
//            if (arr1[i][j] == arr2[i][j])
//            {
//                count++;
//            }
//        }
//    }
//    double smi = 0.0;
//    smi = (count * 1.0) / (h * l) * 100.0;
//    printf("%.2lf", smi);
//    return 0;
//}
//BC80 登录验证

//#include<stdio.h>
//#include<string.h>
//int main()
//{
//    char str1[100] = { 0 };
//    char str2[100] = { 0 };
//    while (~scanf("%s%s", str1, str2))
//    {
//        if (strcmp(str1, str2) == 0 && strcmp(str1, "admin") == 0)
//        {
//            printf("Login Success!\n");
//        }
//        else {
//            printf("Login Fail!\n");
//        }
//    }
//}

//int main()
//{
//	char arr[100] = { 0 };
//	scanf("%s", arr);
//	printf("%s", arr);
//
//}


//BC81 学好C++
//#include<stdio.h>
//int main()
//{
//    printf("I will learn C++ well!");
//}




//BC82 (a+b-c)*d的计算问题
//#include<stdio.h>
//int main()
//{
//    int a, b, c, d;
//    scanf("%d%d%d%d", &a, &b, &c, &d);
//    printf("%d", (a + b - c) * d);
//}


//BC83 被5整除问题
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    if (n % 5 == 0)
//    {
//        printf("YES");
//    }
//    else {
//        printf("NO");
//    }
//}

//BC84 计算y的值
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    if (n < 0)
//    {
//        printf("1");
//    }
//    else if (n == 0)
//    {
//        printf("0");
//    }
//    else {
//        printf("-1");
//    }
//}



#include<stdio.h>
int main()
{
    int i = 0;
    int count = 0;
    for (i = 0; i <= 2019; i++)
    {
        if (i % 10 == 9 || )
        {
            count++;
        }
        if (i / 10 == 9) {
            count++;
        }
        if (i / 100 == 9)
        {
            count++;
        }
    }
    printf("%d", count);
}