#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现strlen
#include<string.h>
#include<stdio.h>
//		创建了局部变量来统计！
// 
// 
// 
//int my_strlen(char* p)
//{
//	int count = 0;
//	while (*p++)
//	{
//		count++;
//	}
//	return count;
//}
//

//			不创建局部变量，使用递归的方法来求解！
//int my_strlen(char* p)
//{
//	if (*p)
//	{
//		return 1 + my_strlen(p + 1);
//	}
//	else if (*p == '\0')
//	{
//		return 0;
//	}
//}


//          利用指针-指针的特性来求字符串长度！
//int my_strlen(char* p, int left, int right)
//{
//	return (p + right) - p;
//}
//int main()
//{
//	char arr[] = "abcdefg";
//	printf("%d", strlen(arr));
//	int right = sizeof(arr) / sizeof(*arr) - 1;
//	printf("%d", my_strlen(arr,0,right));
//	return 0;
//}






		//模拟实现strcpy
//char * my_strcpy(char* p2, const char* p1)
//{
//	char* tem=p2;
//	while (*p2++ = *p1++)
//	{
//		;
//	}
//	return tem;
//}
//		//模拟实现strncpy
//char* my_strncpy(char* p2, const char* p1, int num)
//{
//	char* tem = p2;
//	while (num--)
//	{
//		while (*p2++ = *p1++)
//		{
//			;
//		}
//	}
//	return tem;
//}
//int main()
//{
//	char arr1[] = "hello";
//	char arr2[20] = { 0 };
//	printf("%s", strncpy(arr2, arr1,3));
//	//printf("%s",my_strcpy(arr2, arr1));
//}


			//模拟实现strcmp
//#include<assert.h>
//int my_strcmp(const char* e1, const char* e2)
//{
//	assert(e1 && e2);
//	while (*e1 == *e2)
//	{
//		if (*e1 == '\0')
//		{
//			return 0;
//		}
//		e1++;
//		e2++;
//
//	}
//	return *e1 - *e2;
//}
//			//模拟实现strncmp
//			/*第一种！*/
//int my_strcmp(const char* e1, const char* e2,int num)
//{
//	assert(e1 && e2);
//	while (*e1 == *e2&&num--)
//	{
//		if (*e1 == '\0')
//		{
//			return 0;
//		}
//		e1++;
//		e2++;
//
//	}
//	return *e1 - *e2;
//}
//
//			第二种！
//int my_strcmp(const char* e1, const char* e2, int num)
//{
//	while (num--)
//	{
//		if (*e1 != *e2 || *e1 == '\0')
//		{
//			return *e1 - *e2;
//		}
//		return 0;
//	}
//}
//int main()
//{
//	char arr1[] = "abce";
//	char arr2[] = "abcd";
//	//int ret = strcmp(arr1, arr2);
//	int ret = my_strcmp(arr1, arr2,2);
//	printf("%d", ret);
//}


			//模拟实现strcat
//char* my_strcat(char * des, const char * sor,int num)
//{
//	char* tem = des;
//	while (*des)
//	{
//		des++;
//	}
//
//	while (*des++ = *sor++)
//	{
//		;
//	}
//	return tem;
//}
//char* my_strncat(char* des, const char* sor, int num)
//{
//	char* tem = des;
//	while (*des)
//	{
//		des++;
//	}
//
//	for (int i = 0; i < num; i++)
//	{
//		*(des + i) = *(sor + i);
//	}
//
//	return tem;
//}
//int main()
//{
//	char arr1[20] = "Hello ";
//	char arr2[] = "world";
//	//printf("%s",strncat(arr1, arr2,2));
//	printf("%s", my_strncat(arr1, arr2,2));
//}


//
//////				模拟实现strstr
//////char* my_strstr(const char* des, const char* sor)
//////{
//////	char* e1 = NULL;
//////	char* e2 = NULL;
//////	char* p = (char*)des;
//////	while (*p)
//////	{
//////		e1 = p;	 //指针变量p用来存放目标数组与源字符串相同的第一个的地址！
//////		e2 = (char*)sor;	//指针e2用来控制将要所寻找字符串的地址!
//////		while (*e1 && *e2 && *e2 == *e1)
//////		{
//////			e1++;
//////			e2++;
//////		}
//////			if (*e2 == '\0')
//////			{
//////				return p;
//////			}
//////			p++;
//////	}
//////	return NULL;
//////}
//////int main()
//////{
//////	char arr1[] = "abcdef";
//////	char arr2[] = "cde";
//////	char* ret = strstr(arr1, arr2);
//////	char* ret = my_strstr(arr1, arr2);
//////	printf("%s", ret);
//////}


#include<string.h>
int main()
{
	char arr[] = "wangyi@yun.com";
	char buf[30] = { 0 };//wangyi@yun.com
	strcpy(buf, arr);
	const char* p = "@.";
	char* str = NULL;
	for (str = strtok(buf, p); str != NULL; str=strtok(NULL, p))
	{
		printf("%s\n", str);
	}

	return 0;
}
