#define _CRT_SECURE_NO_WARNINGS 1
		//模拟实现memcpy
#include<stdio.h>
#include<string.h>
#include<assert.h>
//char* my_memcpy(void* e1, void* e2, int num)
//{
//	char* ret = (char*)e1;
//	while (num--)
//	{
//		*(char*)e1 = *(char*)e2;
//		((char*)e1)++;
//		((char*)e2)++;
//	}
//	return ret;
//}
//int main()
//{
//	//int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	//int arr2[10] = {0  };
//	char str1[] = "abcdef";
//	char str2[6] = { 0 };
//	//memcpy(arr2, str1, 40);
//	my_memcpy(str2, str1, 6);
//	//for (int i = 0; i < 10; i++)
//	//{
//	//	printf("%d ", arr2[i]);
//	//}
//	for (int i = 0; i < 6; i++)
//	{
//		printf("%c ", str2[i]);
//	}
//	return 0;
//}

					/*模拟实现memmove*/
//首先需要了解memmove与memcpy的区别，实际上二者没有太大的区别，只是在拷贝的过程中，
//若出现了内存重叠的情况下，不能使用memcpy来实现数组拷贝，
//只能使用memmove来进行拷贝，memcpy具备的功能memmove统统具备，
//若给二者打分，memmove100分
//而memcpy60分左右！
//void* my_memmove(void* e1, void* e2, int num)
//{
//	void* ret = e1;
//	if (e1 <e2)
//	{
//		while (num--)
//		{
//			*(char*)e1 = *(char*)e2;
//			e1 = (char*)e1 + 1;
//			e2 = (char*)e2 + 1;
//		}
//	}
//	else
//	{
//		while (num--)
//		{
//			*((char*)e1 + num) = *((char*)e2 + num);
//		}
//	}
//	return ret;
//}
void* my_memmove(void* dest, const void* src, size_t num)
{
	void* ret = dest;
	assert(dest && src);
	if (dest < src)
	{
		//前->后
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	else
	{
		//后->前
		while (num--)
		{
			*((char*)dest + num) = *((char*)src + num);
		}
	}
	return ret;
}
int main()
{
	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
	memmove(arr1+2, arr1, 20);
	for (int i = 0; i < 10; i++)
	{
		printf("%d ", arr1[i]);
	}
	//my_memcpy(arr1+2, arr1, 20);
	my_memmove(arr1+1, arr1, 20);
	printf("\n");
	for (int i = 0; i < 10; i++)
	{
		printf("%d ", arr1[i]);
	}
	//当要拷贝的两端数据有重合的情况下则需要分情况考虑了！
	//1.若目标数组在源数组的前面，则必须从前向后拷贝！若从后向前拷贝，则会将源数据改变！
	//2.若目标数组在源数组的后面且有重合的部分的情况下，则必须从后往前拷贝，否则同样会造成源数据中的内容改变！
	//3.在目标数组在源数组的后面，且没有重合的部分，则怎么拷贝都行！
	//为了方便起见，仅需分两种情况即可，即1为第一种，23为第二种！
	//只需考虑目标数组是否在源数组的前面，若在则进行从前向后拷贝，否则就是从后向前拷贝！

}
