#define _CRT_SECURE_NO_WARNINGS 1


//BC90 矩阵计算
//#include<stdio.h>
//int main()
//{
//    int x = 0;
//    int y = 0;
//    int arr[10][10];
//    int sum = 0;
//    int j = 0;
//    int i = 0;
//    scanf("%d %d", &x, &y);
//    for (i = 0; i < x; i++)
//    {
//        for (j = 0; j < y; j++)
//        {
//            scanf(" %d", &arr[i][j]);
//            if (arr[i][j] >= 0)
//            {
//                sum += arr[i][j];
//            }
//        }
//    }
//    printf("%d", sum);
//    return 0;
//}


//BC91 成绩输入输出问题
//#include<stdio.h>
//int main()
//{
//    int arr[10] = { 0 };
//    int i = 0;
//    for (i = 0; i < 10; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    for (i = 0; i < 10; i++)
//    {
//
//        printf("%d ", arr[i]);
//    }
//}

//BC92 逆序输出
//#include<stdio.h>
//int main()
//{
//    int i = 0;
//    int arr[10] = { 0 };
//    for (i = 0; i < 10; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    for (i = 9; i >= 0; i--)
//    {
//        printf("%d ", arr[i]);
//    }
//}


//BC93 统计数据正负个数
//#include<stdio.h>
//int main()
//{
//    int arr[10] = { 0 };
//    int i = 0;
//    int add = 0;
//    int odd = 0;
//    for (i = 0; i < 10; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    for (i = 0; i < 10; i++)
//    {
//        if (arr[i] >= 0)
//        {
//            odd++;
//        }
//        else {
//            add++;
//        }
//    }
//    printf("positive:%d\nnegative:%d", odd, add);
//}



//BC94 N个数之和
//#include <stdio.h>
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int sum = 0;
//    int arr[n];
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//        sum += arr[i];
//    }
//    printf("%d", sum);
//}

//BC95 最高分与最低分之差
// 
// 
//#include<stdio.h>
//#include<stdlib.h>
//int cmp(void* e1, void* e2)
//{
//    return *(int*)e1 - *(int*)e2;
//}
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int arr[n];
//    int i = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    qsort(arr, n, sizeof(arr[0]), cmp);
//    printf("%d", arr[n - 1] - arr[0]);
//}

//#include <stdio.h>
//int main()
//{
//    int a[55], n, flag1 = 0, flag2 = 0, i;
//    scanf("%d", &n);
//    for (i = 0; i < n; i++) {
//        scanf("%d", &a[i]);
//        if (i > 0) {
//            if (a[i] < a[i - 1]) {
//                flag1 = 1;
//            }
//            else if (a[i] > a[i - 1]) {
//                flag2 = 1;
//            }
//        }
//    }
//    if (flag1 && flag2) printf("unsorted\n");//只有当flag1和flag2都为1的时候序列无序
//    else printf("sorted\n");
//}

//BC76 公务员面试

#include <stdio.h>
#include<string.h>
#include<stdlib.h>
//void bubble_sort(int arr[],int sz)
//{
//    int i=0;
//    int j=0;
//    for(i=0;i<sz-1;i++)
//    {
//        for(j=0;j<sz-1-i;j++)
//        {
//            if(arr[j]<arr[j+1])
//            {
//                int tem=arr[j];
//                arr[j]=arr[j+1];
//                arr[j+1]=tem;
//            }
//        }
//    }
//}
//int cmp(void* e1, void* e2)
//{
//    return *(int*)e1 - *(int*)e2;
//}
//int main()
//{
//    int arr[7] = { 0 };
//    int i = 0;
//    int sz = 7;
//
//    double ave = 0.0f;
//    while (~scanf("\n%d%d%d%d%d%d%d",
//        &arr[0], &arr[1], &arr[2], &arr[3], &arr[4], &arr[5], &arr[6]))
//    {
//        int sum = 0;
//        qsort(arr, sz, sizeof(int), cmp);
//        for (i = 1; i < sz - 1; i++)
//        {
//            sum += arr[i];
//        }
//        printf("%.2lf\n", sum / 5.0);
//    }
//    //bubble_sort(arr,sz);
//
//}

//BC11 学生基本信息输入输出
//#include<stdio.h>
//int main()
//{
//    int num = 0;
//    long double a = 0.0f;
//    long double b = 0.0f;
//    long double c = 0.0f;
//    scanf("%d;%llf,%llf,%llf", &num, &a, &b, &c);
//    printf("The each subject score of No. %d is %.2llf, %.2llf, %.2llf.", num, a, b, c);
//
//}

//BC21 浮点数的个位数字
//#include<stdio.h>
//int main()
//{
//    float a = 0;
//    scanf("%f", &a);
//    int i = 0;
//    i = (int)a % 10;
//    printf("%d", i);
//
//}