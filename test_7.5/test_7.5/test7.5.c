#define _CRT_SECURE_NO_WARNINGS 1
//#include <stdio.h>
//void func(char* p) { p = p + 1; }
//int main()
//{
//	char s[] = { '1', '2', '3', '4' };
//	func(s);
//	printf("%c", *s);
//	return 0;
//}

#include<stdio.h>
#include<math.h>
int Ws(int x)
{
    int count = 1;
    if (x > 9)
    {
        x /= 10;
        count++;
    }
    return count;
}
int main()
{
    int n = 0;
    int zs = 1; //表示自守数的个数！
    scanf("%d", &n);
    for (int i = 1; i <= n; i++)
    {
        //该问题核心主要是确定i是几位数，然后根据
        //其位数可以进行取余操作得到答案
        int count = Ws(i);   //count 得到的是一个数的位数！
        int pf = pow(i, 2);    //pf为一个数的平方！
        if (pf % ((count - 1) * 10) == i)
        {
            zs++;
        }
    }
    printf("%d", zs);
    return 0;
}