#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
//首先用qsort函数测试整形的排序！
struct stu
{
	char name[20];
	int age;
};
void print(int arr[], int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ",arr[i]);
	}
}
int cmp_int(const void* e1, const void* e2)
{
	return *(int*)e1 - *(int*)e2;
}
int cmp_struct_age(const void* e1, const void* e2)
{
	return ((struct stu*)e1)->age - ((struct stu*)e2)->age;
}
int cmp_struct_name(const void* e1, const void* e2)
{
	strcmp(((struct stu*)e1)->name,((struct stu*)e2)->name);
}
void test1()
{
	int arr[10] = { 5,2,1,3,0,4,6,7,8,9 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, sz,sizeof(arr[0]),cmp_int);  //qsort函数中参数分别为：（数组第一个元素的地址，数组中需要排序的个数，元素的大小，比较函数！）
	print(arr, sz);
}
void test2()
{

	struct stu s [] = {{"zhangsan",25},{"lisi ",20},{"wangwu ",30}};
	int sz = sizeof(s) / sizeof(s[0]);
	/*qsort(s, sizeof(s) / sizeof(s[0]), sizeof(s[0]), cmp_struct_age);*/
	qsort(s, sizeof(s) / sizeof(s[0]), sizeof(s[0]), cmp_struct_name);
	print(s , sz);

}
int main()
{
	/*test1();*/
	test2();
}