#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"
void Init(char board[ROW][COL],int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < ROW; i++)
	{
		j = 0;
		for (j = 0; j < COL; j++)
		{
			board[i][j] = ' ';
		}
	}
}
 void Display (char board[ROW][COL], int row, int col)
{
	 int i = 0;
	 for (i = 0; i < ROW; i++)
	 {
		 /*打印数据*/
		 int j = 0;
		 for (j = 0; j < COL; j++)
		 {
			 printf(" %c ", board[i][j]);
			 if (j < COL - 1)
			 {
				 printf("|");
			 }
		 }
		 printf("\n");
		 /*打印分解符*/
		 for (j = 0; j < ROW; j++)
		 {
			 printf("---");
			 if (j < ROW - 1)
			 {
				 printf("|");
			 }
		 }
		 printf("\n");
	 }
}
 void Playermove(char board[ROW][COL], int row, int col)
 {
	 printf("\n玩家下棋\n");
	 int x = 0;
	 int y = 0;
	 while (1)
	 {
		 printf("请输入坐标，两个数字之间用空格间隔->");
		 scanf("%d %d",&x, &y);
		 if (x >= 1 && x <= 3 && y >= 1 && y <= 3)
		 {
			 if (board[x - 1][y - 1] == ' ')
			 {
				 board[x-1][y-1] = '*';
				 break;
			 }
			 else
			 {
				 printf("\n已有落子重新选择\n");
			 }
		 }
		 else
		 {
			 printf("坐标错误，重新选择\n");
		 }
	 }
 }
 void Computermove(char board[ROW][COL], int row, int col)
 {
	 printf("\n电脑下棋\n");
	 while (1)
	 {
		 int x = rand() % ROW;
		 int y = rand() % COL;
		 if (board[x][y] == ' ')
		 {
			 board[x][y] = '#';
			 break;
		 }
	 }
 }
 int  Isfull(char board[ROW][COL], int row, int col)
 {
	 int flag = 0;
	 int  i = 0;
	 for (i = 0; i < ROW; i++)
	 {
		 int j = 0;
		 for (j = 0; j < COL; j++)
		 {
			 if (board[i][j] == ' ')
				 return 1;
		 }
	 }
	 return 0;
	 			/*o表示棋盘满了*/
 }
 char Iswin(char board[ROW][COL], int row, int col)
 {
	 int i = 0;
	 for(i=0;i<ROW;i++)
	 {
		 /*行相同*/
		 if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != ' ')
			 return board[i][0];
	 }
	 for (i = 0; i < COL; i++)
	 {
		 /*列相同*/
		 if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] != ' ')
			 return board[0][i];
	 }
				/*对角线相同*/
	 if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != ' ')
		 return board[0][0];
	 if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ')
		 return board[0][2];
					/*(表示继续下棋)*/
	 if (Isfull(board, row, col) == 0)
	 {
		 return 'Q';			/*平局*/
	 }
	 return 'C';				/*继续*/
 }