#define COL 3
#define ROW 3
#include<time.h>
#include<stdlib.h>
void Init(char board[ROW][COL],int row,int col);
void Display(char board[ROW][COL], int row, int col);
void Playermove(char board[ROW][COL], int row, int col);
void Computermove(char board[ROW][COL], int row, int col);
int  Isfull(char board[ROW][COL], int row, int col);
char Iswin(char board[ROW][COL], int row, int col);



