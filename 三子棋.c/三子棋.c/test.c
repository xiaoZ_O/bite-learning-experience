#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include"game.h"
				///*三子棋游戏的实现
				//首先先打印菜单
				//玩家进入选择，选择进行游戏或者退出游戏，或者玩家输入错误，提示。
				//若玩家选择进入游戏，则进行game()，编写game函数内部的内容。
void game()
{
	char board[ROW][COL] = { 0 };
	//printf("玩家下棋\n");
	Init(board, ROW, COL);
	Display(board, ROW, COL);
	char ret = 0;
	while (1)
	{
		Playermove(board, ROW, COL);
		Display(board, ROW, COL);
		ret = Iswin(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
		Computermove(board, ROW, COL);
		Display(board, ROW, COL);
		ret = Iswin(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
	}
	if (ret == '*')
	{
		printf("玩家赢\n");
	}
	else if (ret == '#')
	{
		printf("电脑赢\n");
	}
	else
	{
		printf("平局\n");
	}
}
void Menu()
{
	printf("***************************\n");
	printf("***1.play    0.exit********\n");
	printf("***************************\n");
	printf("***************************\n");
}
int main()
{
	int input = 0;
	srand((unsigned int )time(NULL));
	
	do
	{
		Menu();
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:printf("退出游戏\n");
			break;
		default:printf("输入错误，请重新选择\n");
		}
		
	} while (input);
}