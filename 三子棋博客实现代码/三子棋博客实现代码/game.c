#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include"game.h"
void Init(char board[ROW][COL], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)			
		{
			board[i][j] = ' ';				/*将数组board中的每个元素都赋予空格*/
		} 
	}
}
void Display(char board[ROW][COL], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)				/*控制整体的行数*/
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			if (j< row)
			{
				printf(" %c ", board[i][j]);
			}
			if (j < row - 1)				/*在最后一列不需要再打印一次|了*/
			{
				printf("|");
			}
		}
		printf("\n");							/*每打印一行，进行一次换行*/
		for (j = 0; j < row; j++)
		{
			if (j < row)
			{
			printf("---");
			}
			if (j < col - 1)
			{
				printf("|");				/*在最后一列不需要再打印一次|了*/
			}
		}
		printf("\n");
	}
}
void Playermove(char board[ROW][COL], int row, int col)
{
	int x = 0;
	int y = 0;
	printf("玩家请输入落棋的坐标：\n");
	while (1)
	{
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= 3 && y >= 1 && y <= 3)
		{
			if (board[x - 1][y - 1] == ' ')
			{
				board[x - 1][y - 1] = '*';
				break;
			}
			else
			{
				printf("该坐标已落棋，请重新选择\n");
			}
		}
		else
		{
			printf("坐标非法，重新选择\n");
		}
	}
}
void Computermove(char board[ROW][COL], int row, int col)
{
	printf("电脑下棋\n");
	int x = 0;
	int y = 0;
	while (1)
	{
		x = rand() % ROW;			/*因为电脑的x,y取值范围为0到2，所以不用再-1,*/
		y = rand() % COL;			/*只需判断是否可以落子即可，进行循环判断就行*/
		
		if (board[x][y] == ' ')
		{
			board[x][y] = '#';		/*电脑下棋用#表示*/
			break;
		}
	}
}
int Full(char board[ROW][COL], int row, int col)
{
	int flag = 0;  /*利用flag来标志着棋盘满了的情况*/
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			if (board[i][j] == ' ')
			{
				flag = 1;		/*flag=1表示棋盘未满的情况*/
				return flag;
			}
		}
	}
	return 0;
}
char Win(char board[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	/*行赢的情况*/
	for (i = 0; i < row; i++)
	{
		if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != ' ')
			return board[i][0];
	}
	/*列赢的情况*/
	for (j = 0; j < col; j++)
	{
		if (board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[0][j] != ' ')
			return board[0][j];
	}
		/*对角线赢的情况*/
	if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != ' ')
			return board[0][0];
	else if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != ' ')
			return board[0][2];
	else if (Full(board, ROW, COL) == 0)
		return 'Q';			/*表示平局*/
	else
	{
		return 'C';
	}
}		
