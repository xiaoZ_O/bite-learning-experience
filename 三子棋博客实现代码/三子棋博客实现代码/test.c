#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include"game.h"
//首先先来实现目录打印。
void menu()
{
	printf("*******************************\n");
	printf("**1.play      0.exit***********\n");
	printf("*******************************\n");
	printf("*******************************\n");
}
void game()
{
	char ret = 0;
	char board[ROW][COL] = { 0 };
	Init(board,ROW,COL);
	Display(board, ROW, COL);
	while (1)
	{										/*进行循环下棋，直接棋盘满了为止。*/
		Playermove(board, ROW, COL);
		Display(board, ROW, COL);
		ret = Win(board, ROW, COL);
		if (ret != 'C')						/*此语句是为了判断棋盘是否满了，循环是否该于此结束*/
			break;
		Computermove(board, ROW, COL);
		Display(board, ROW, COL);
		ret = Win(board, ROW, COL);
		if (ret != 'C')
			break;
	}
	if (ret == '*')
	{										/*最后再对ret的返回值进行判断，以此得出结果如何。*/
		printf("玩家赢\n");
	}
	else if (ret == '#')
	{
		printf("电脑赢\n");
	}
	else
	{
		printf("平局\n");
	}
}
int main()
{
	srand((unsigned int) time(NULL));
	int input = 0;
	do
	{
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case 0:printf("退出游戏\n");
			break;
		case 1:
			printf("三子棋\n");
			game();
			break;
		default:
			printf("输入错误，请重新选择\n");
			break;
		}
	} while (input);
}
