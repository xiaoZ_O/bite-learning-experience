#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

   /* 将两个有序数组进行合并成一个有序数组*/
int main()
    {
        int arr1[500];
        int arr2[500];
        int i = 0;
        int m = 0;
        int n = 0;
        int j = 0;
        scanf("%d%d", &m, &n);
        for (i = 0; i < m; i++)
        {
            scanf("%d", &arr1[i]);
        }
        for (j = 0; j < n; j++)
        {
            scanf("%d", &arr2[j]);
        }
        i = 0;
        j = 0;
        while (i < m && j < n)
        {

            if (arr1[i] < arr2[j])
            {
                printf("%d ", arr1[i]);
                i++;
            }
            else
            {
                printf("%d ", arr2[j]);
                j++;
            }
        }
        while (i < m || j < n)
        {
            if (j < n && i == m)
            {
                printf("%d ", arr2[j]);
                j++;
            }
            else
            {
                printf("%d ", arr1[i]);
                i++;
            }
        }
        return 0;
}