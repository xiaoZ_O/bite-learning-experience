#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>
		/*判断回文素数，首先先判断是否为素数，在已经确定为素数的前提下然后再进行判断是否回文，
		满足回文条件的两位数是其个位与十位数字相同，三位数是其百位数和个位数数字相同，
		所以首先先来添加一个flag标签来表示是否满足素数，然后再判断其是否满足回文即可*/
int main()
{
	int i = 0;
	int n = 1000;
	int j = 0;
	int flag = 0;
	for (i = 10; i <=n; i++)
	{
		int flag = 1;
		for ( j= 2; j< sqrt(i); j++)
		{
			if (i % j == 0)
			{
				flag = 0;
			}
			break;
		}
		if (flag == 1)
		{
			if (i < 100)
			{
				if (i / 10 == i % 10)
				{
					printf("%4d\t", i);
				}
			}
				else
			{
				if (i / 100 == i % 10)
				{
					printf("%4d\t", i);
				}	
			}
		}
	}
	return 0;
	
}