#define _CRT_SECURE_NO_WARNINGS 1
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
				/*求两个数的最小公倍数*/
//1.暴力枚举法：从两个数中的最大值开始循环，直至找到能同时整除两个的元素！利用每次加一！
//2.对法一的改进：两个数中的最大开始，×i，直至找到a*i能整除b的元素！
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d%d", &a, &b);
//	int max = a > b ? a : b;
//	while (1)
//	{
//		if (max % a == 0 && max % b == 0)
//		{
//			break;
//		}
//		max++;
//	}
//	printf("%d", max);
//	return 0;
//}
//2.对法一的改进：两个数中的最大开始，×i，直至找到a * i能整除b的元素！
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d%d", &a, &b);
//	int max = 0;
//	int min = 0;
//	if (a > b)
//	{
//		max = a;
//		min = b;
//	}
//	else
//	{
//		max = b;
//		min = a;
//	}
//
//	int i = 0;
//	while (1)
//	{
//		if (max*i%min==0)
//		{
//			break;
//		}
//		i++;
//	}
//	printf("%d", max*i);
//	return 0;
//}

//int main()
//{
//	int a = 3;
//	int* p = &a;
//	printf("%d", *p++);
//	return 0;
//}


//将一句话的单词进行倒置，标点不倒置。比如 "I like beijing."，经过处理后变为："beijing. like I"。
//字符串长度不超过100。

//首先先实现字符串的逆序，然后再进行字母的逆序，字母的逆序也可以利用reverse函数来实现！
//#include<stdlib.h>
//void reverse(char* left, char*right)
//{
//	while (left<right)
//	{
//		char tmp=*left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//int main()
//{
//	char arr[101]={0};
//	gets(arr);
//	int len = strlen(arr)-1;
//	reverse(arr, arr+len);
//	/*printf("%s", &arr);*/
//	char* start = arr;
//	char* cur = arr;
//	while (*cur!='\0')
//	{
//		while (*cur != ' ' && *cur != '\0')
//		{
//			cur++;
//		}
//		reverse(start, cur - 1);
//		start = cur + 1;
//		if (*cur == ' ')
//		{
//			cur++;
//		}
//	}
//	printf("%s\n", arr);
//	/*char* start = arr;
//	char* cur = arr;*/
//
//	return 0;
//}



//将一句话的单词进行倒置，标点不倒置。比如 "I like beijing."，经过处理后变为："beijing. like I"。
//字符串长度不超过100。

//首先先实现字符串的逆序，然后再进行字母的逆序，字母的逆序也可以利用reverse函数来实现！
#include<stdlib.h>
//void reverse(char* left, char* right)
//{
//	while (left < right)
//	{
//		char tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//int main()
//{
//	char arr[101];
//	gets(arr);
//	int len = strlen(arr) - 1;
//	reverse(arr, arr + len);
//	//printf("%s", arr);		/*先来检测reverse函数是否写的正确*/   
//	/*将整个字符串逆序过后然后再考虑字母的逆序*/
//	char* start = arr;
//	char* cur = arr;
//	while (*cur != '\0')			/*当cur指向的是字符串结束标志‘\0’时结束循环！*/
//	{
//		while (*cur != ' ' && *cur != '\0')		/*当cur指向的元素不是空格和\0时，cur后移，直至找到空格！*/
//		{
//			cur++;
//		}
//		reverse(start,cur-1);				/*将首元素和cur之前这些字母逆序，实现字母的逆序！*/
//		start = cur + 1;					/*然后再重新更新首地址！*/
//		if (*cur == ' ')					/*只有当cur指向的元素是空格时再进行+1,当cur指向字符串结束标志\0时，再使用+1，就会导致死循环!*/
//			cur++;
//	}
//	printf("%s", arr);
//	return 0;
//}{

int main()
{
	int arr1[]=;
	int arr2[];
}