#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//int main()
//{
//	int a[5] = { 1, 2, 3, 4, 5 };
//	int* ptr = (int*)(&a + 1);
//	printf("%d,%d", *(a + 1), *(ptr - 1));
//	return 0;
//}

//struct Test
//{
//	int Num;
//	char* pcName;
//	short sDate;
//	char cha[2];
//	short sBa[4];
//}*p;
//假设p 的值为0x100000。 如下表表达式的值分别为多少？
//已知，结构体Test类型的变量大小是20个字节
//int main()
//{
//	printf("%p\n", p + 0x1);		//+20
//	printf("%p\n", (unsigned long)p + 0x1);   //+1
//	printf("%p\n", (unsigned int*)p + 0x1);		//+4
//	return 0;
//}


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
int main()
{
	int a[4] = { 1, 2, 3, 4 };
	int* ptr1 = (int*)(&a + 1);
	int* ptr2 = (int*)((int)a + 1);
	printf("%x,%x", ptr1[-1], *ptr2);  //4   0x02000000
	return 0;
}



//#include <stdio.h>
//int main()
//{
//	int a[3][2] = { (0, 1), (2, 3), (4, 5) };   1  3  5  0   0   0
//	int* p;
//	p = a[0];
//	printf("%d", p[0]);  //1    p[0]=*(p+0)      
//	return 0;
//}


//int main()
//{
//	int a[5][5];
//	int(*p)[4];
//	p = a;
//	printf("%p,%d\n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]);//两个地址相减得到的是之间元素的个数！  
//  若打印地址，打印的则是在该内存中存储的数字的补码！
//	return 0;
//}


//int main()
//{
//	int aa[2][5] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	int* ptr1 = (int*)(&aa + 1);
//	int* ptr2 = (int*)(*(aa + 1));   //二维数组名表示第一行的地址!进行加+1则跳过第一行，进入第二行，再进行解引用，表示存储的·是6的地址！
//	printf("%d,%d", *(ptr1 - 1), *(ptr2 - 1));			//10   5
//	return 0;
//}


//#include <stdio.h>
//int main()
//{
//	char* a[] = { "work","at","alibaba" };
//	char** pa = a;
//	pa++;					pa指向的是数组a的地址，pa++则指针向下移动一个char*，指向存放at的地址，再次解引用就可以得到at!
//	printf("%s\n", *pa);			//at
//	return 0;
//}


//int main()
//{
//	char* c[] = { "ENTER","NEW","POINT","FIRST" };
//	char** cp[] = { c + 3,c + 2,c + 1,c };
//	char*** cpp = cp;
//	printf("%s\n", **++cpp);
//	printf("%s\n", *-- * ++cpp + 3);
//	printf("%s\n", *cpp[-2] + 3);
//	printf("%s\n", cpp[-1][-1] + 1);
//	return 0;
//}