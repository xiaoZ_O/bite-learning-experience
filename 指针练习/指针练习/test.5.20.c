#define _CRT_SECURE_NO_WARNINGS 1
			/*写一个函数打印arr数组的内容，不使用数组下标，使用指针。
			arr是一个整形一维数组。*/
//#include<stdio.h>
//int main()
//{
//
//	int i = 0;
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = &arr;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}


/*实现字符串的逆序，字符串可能存在空格*/
#include<string.h>
#include<stdlib.h>
#include<stdio.h>
//int my_strlen(char arr[])
//{
//	int i = 0;
//	int count = 0;
//	while (arr[i] != '\0')
//	{
//		count++;
//		i++;
//	}
//	return count;
//}
//
//int main()
//{
//	int sz = 0;
//	int i = 0;
//	char arr[1999] = { 0 };
//	gets(arr);
//	sz = my_strlen(arr);
//	for (i = sz - 1; i >= 0; i--)
//	{
//		printf("%c", arr[i]);
//	}
//	return 0;
//}

//int main()
//{
//	printf("      *      \n");
//	printf("     ***     \n");
//	printf("    *****    \n");
//	printf("   *******   \n");
//	printf("  *********  \n");
//	printf(" *********** \n");
//	printf("*************\n");
//	printf(" *********** \n");
//	printf("  *********  \n");
//	printf("   *******   \n");
//	printf("    *****    \n");
//	printf("     ***     \n");
//	printf("      *      \n");
//	return 0;
//}

//求出0～100000之间的所有“水仙花数”并输出。
//
//“水仙花数”是指一个n位数，其各位数字的n次方之和确好等于该数本身，如 : 153＝1 ^ 3＋5 ^ 3＋3 ^ 3，则153是一个“水仙花数”。
//#include<math.h>
//int main()
//{
//	int i = 0;
//	for (i = 1; i <= 100000; i++)
//	{
//		if (i < 100)							/*两位数符合的情况*/
//		{
//			int g = i % 10;		/*个位数字；*/
//			int s = i / 10 % 10;		/*十位数字；*/
//			if (pow(g,2)+pow(s,2)==i)
//			{
//				printf("%d\t", i);
//			}
//		}
//		if (i >= 100 && i < 1000)			/*	三位数符合的情况*/
//		{
//			int g = i % 10;		/*个位数字；*/
//			int s = i / 10 % 10;		/*十位数字；*/
//			int b = i / 100 % 10;       /*百位数字*/
//			if (pow(g, 3) + pow(s, 3) + pow(b, 3) == i)
//			{
//				printf("%d\t", i);
//			}
//		}
//		if (i >= 1000 && i < 10000)					/*	四位数符合的情况*/
//		{
//			int g = i % 10;		/*个位数字；*/
//			int s = i / 10 % 10;		/*十位数字；*/
//			int b = i / 100 % 10;       /*百位数字*/
//			int q = i / 1000 % 10;		/*千位数字*/
//			if (pow(g, 4) + pow(s, 4) + pow(b, 4) + pow(q, 4) == i)
//			{
//				printf("%d\t", i);
//			}
//		}
//		if (i >= 10000 && i < 100000)				/*	五位数符合的情况*/
//		{
//			int g = i % 10;		/*个位数字；*/
//			int s = i / 10 % 10;		/*十位数字；*/
//			int b = i / 100 % 10;       /*百位数字*/
//			int q = i / 1000 % 10;		/*千位数字*/
//			int w = i / 10000 % 10;     /*万位数字;*/
//			if (pow(g, 5) + pow(s, 5) + pow(b, 5) + pow(q, 5) + pow(w, 5) == i)
//			{
//				printf("%d\t", i);
//			}
//		}
//	}
//	return 0;
//
//}



////求Sn = a + aa + aaa + aaaa + aaaaa的前5项之和，其中a是一个数字，
////
////例如：2 + 22 + 222 + 2222 + 22222
//int main()
//{
//	printf("请输入一个数，求其前五位之和：");
//	int g = 0;
//	scanf("%d", &g);
//	int s = 10 * g + g;
//	int b = 100 * g + s;
//	int q = 1000 * g + b;
//	int w = 10000 * g + q;
//	int sum = g + s + b + q + w;
//	printf("%d", sum);
//	return 0;
//}
#include <stdio.h>

//int main()
//{
//    int n = 0;
//    int i = 0;
//    int flag = 1;
//    scanf("%d", &n);
//    int arr[200] = { 0 };
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    if (arr[0] < arr[n - 1])        //将第一个数和最后一个数比较，确定升降序
//    {
//        int j = 0;
//        for (j = 0; j < n - 1; j++)
//        {
//            if (arr[j] > arr[j + 1])
//            {
//                flag = 0;
//                break;
//            }
//        }
//    }
//    if (arr[0] > arr[n - 1])
//    {
//        int j = 0;
//        for (j = 0; j < n - 1; j++)
//        {
//            if (arr[j] < arr[j + 1])
//            {
//                flag = 0;
//                break;
//            }
//        }
//    }
//    if (arr[0] == arr[n - 1])
//    {
//        for (j = 0; j <= n - 1; j++)
//        {
//            if (arr[j] != arr[j + 1])
//            {
//                flag = 0;
//                break;
//            }
//        }
//    }
//    if (flag == 0)
//    {
//        printf("unsorted");
//    }
//    else
//    {
//        printf("sorted");
//    }
//    return 0;
//}

int main()
{
    int n = 0;
    int i = 0;
    int flag = 1;                       //1代表有序，0代表无序
    scanf("%d", &n);
    int arr[500] = { 0 };
    for (i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
    }
    if (arr[0] < arr[n - 1])        //将第一个数和最后一个数比较，确定升降序
    {
        int j = 0;
        for (j = 0; j <= n - 2; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                flag = 0;
                break;
            }
        }
    }
    if (arr[0] > arr[n - 1])
    {
        int j = 0;
        for (j = 0; j <= n - 2; j++)
        {
            if (arr[j] < arr[j + 1])
            {
                flag = 0;
                break;
            }
        }
    }
    if (arr[0] == arr[n - 1])
    {
        int j = 0;
        for (j = 0; j <= n - 2; j++)
        {
            if (arr[j] != arr[j + 1])
            {
                flag = 0;
                break;
            }
        }
    }
    if (flag == 0)
    {
        printf("unsorted");
    }
    else
    {
        printf("sorted");
    }
    return 0;
}