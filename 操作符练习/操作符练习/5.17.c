#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
										//不允许创建临时变量，交换两个整数的内容
// 1.利用异或解决
//int main()
//{
//	int a = 6;
//	int b = 8;
//	a = a ^ b;			/*变量a用来存储a^b的值；*/
//	b = a ^ b;			/*b = a ^ b ^ b, 两个相同的变量异或得到的是0, 0与任何数异或得到的那个数本身，所以 b = a;*/
//	a = a ^ b;			/*a=a^b^a,又因为异或满足交换律，所以得到的结果是b；*/
//	printf("a=%d\nb=%d", a, b);
//	return 0;
//}
//利用加减法解决
//int main()
//{
//	int a = 5;
//	int b = 6;
//	a = a + b;
//	b = a - b;
//	a = a - b;
//	printf("a=%d\nb=%d", a, b);
//	return 0;
//}



							/*输入一个整数 n ，输出该数32位二进制表示中1的个数。其中负数用补码表示。*/


#include <stdio.h>
//int main()
//{
//	int num1 = 1;			/*00000001*/
//	int num2 = 2;			/*00000010	*/
//	num1& num2;				/*00000000	0*/
//	num1 | num2;			/*00000011	3*/
//	num1^ num2;				/*00000011	3*/
//	printf("%d %d %d", num1 & num2, num1 | num2, num1 ^ num2);
//	return 0;
//}


//#include <stdio.h>
//int main()
//{
//	int a = -10;
//	int* p = NULL;
//	printf("%d\n", !2);					/*0*/
//	printf("%d\n", !0);					/*1*/
//	a = -a;								/*10*/
//	p = &a;
//	printf("%d\n", sizeof(a));				/*4*/
//	printf("%d\n", sizeof(int));			/*4*/
//	//printf("%d\n", sizeof a);//这样写行不行？			4
//	//printf("%d\n", sizeof int);//这样写行不行？   不行，只有输入表达式才可以省略括号
//	return 0;
//}



//#include <stdio.h>
//void test1()
//{
//	printf("hehe\n");
//}
//void test2(const char* str)
//{
//	printf("%s\n", str);
// 
//int main()
//{
//	test1();            //实用（）作为函数调用操作符。
//	test2("hello bit.");//实用（）作为函数调用操作符。
//	return 0;
//}


						/*.  操作符的介绍*/
//struct book
//{
//	char name[20];
//	char author[5];
//	float price;
//};
//int main()
//{
//	struct book b1 = { "dsakdas","六六",8.68f };
//	struct book b2 = { "chushuc","七七",9.96 };
//	printf("%s %s %f ", b1.name, b1.author, b1.price);
//	printf("%s %s %f ", b1.name, b1.author, b1.price);
//}


					/*->操作符的介绍*/
//struct book
//{
//	char name[20];
//	char author[5];
//	float price;
//};
//void print(struct book *pb)
//{
//	printf("%s %s %f \n", (*pb) .name, (*pb).author, (*pb).price);
//	printf("%s %s %f \n", pb->name, pb->author, pb->price);
//}
//int main()
//{
//	struct book b1 = { "dsakdas","六六",8.68f };
//	struct book b2 = { "chushuc","七七",9.96 };
//	print(&b1);
//	print(&b2);
//}


//int main()
//{
//	float q = 6.0f;
//	int s = 2;
//	float m = q / s;
//	printf("%lf\n", m);			/*浮点数除法的例子*/
//	int a = 6;
//	int b = 2;
//	int c = a / b;
//	printf("%d", c);			/*整数除法的例子*/
//	return 0;
//}

//int main()
//{
//	int a = 2;	/* 00000000000000000000000000000010*/
//	int b = 1;	/* 00000000000000000000000000000001*/
//	int c = a&b; /*00000000000000000000000000000000*/    /*执行按位与操作符时，只有同时为1的情况才为1！所以结果是0*/
//	int s = a | b;/*00000000000000000000000000000011*/		/*执行按位或操作符时，有1就为1，全0才为0！所以结果是3*/
//	int q = a ^ b;/*00000000000000000000000000000011*/    /*执行按位异或操作符时，相同为0，相异为1，所以结果是3*/
//	printf("%d\n", c);
//	printf("%d\n", s);
//	printf("%d\n", q);
//	return 0;
//}
// 
// 
// 
			//逻辑与和逻辑或
//int main()
//{
//	int a = 2;  
//	int b = 0;	
//	int c = a && b;
//	int s = 0;
//	int q = 1;
//	int m = s||q;
//	printf("%d\n", m);			/*结果为1*/
//	printf("%d\n", c);			/*结果为0*/
//	return 0;
//}

							//移位操作符
//int main()
//{
//	int a = 2;						/*00000000000000000000000000000010*/
//	int c = a << 1;					/*00000000000000000000000000000100*/
//	int d = a >> 1;					/*00000000000000000000000000000001*/
//	printf("%d\n", c);			/*结果为4*/ 
//	printf("%d\n", d);			/*结果为1*/
//	return 0;
//}
					//360面试题目（&& ||）的使用
//#include <stdio.h>
//int main()
//{
//	int i = 0, a = 0, b = 2, c = 3, d = 4;
//	i = a++ && ++b && d++;
//	//i = a++||++b||d++;
//	printf("a = %d\n b = %d\n c = %d\nd = %d\n", a, b, c, d);
//	return 0;
//}


//int main()
//{
//	int n = 10;
//	char* pc = (char*)&n;
//	int* pi = &n;
//
//	printf("%p\n", &n);				/*1*/
//	printf("%p\n", pc);				/*1*/
//	printf("%p\n", pc + 1);			/*1+1*/
//	printf("%p\n", pi);				/*1*/
//	printf("%p\n", pi + 1);			/*1+4*/
//	return  0;
//}



//#include <stdio.h>
//int main()
//{
//	int n = 0x11223344;
//	char* pc = (char*)&n;
//	int* pi = &n;
//	*pc = 0;   //重点在调试的过程中观察内存的变化。
//	*pi = 0;   //重点在调试的过程中观察内存的变化。
//	return 0;
//}

                    /*计算二进制位中一的个数*/
//int main()
//{
//    /*数字每次向右移动一位*/
//    int i = 0;
//    int count = 0;
//    int num = 0;
//    scanf("%d", &num);
//    for (i = 0; i < 32; i++)
//    {
//        if (1 == ((num >>i) & 1))
//        {
//            count++;
//        }
//    }
//    printf("%d", count);
//    return 0;
//}


//按位异或^实现两个数的交换
//int main()
//{
//	int a = 2;		/*a	00000000000000000000000000000010;*/
//	int b = 4;		/*b	00000000000000000000000000000100;*/
//	a = a ^ b;		/*a = 00000000000000000000000000000110;*/
//					/*b	00000000000000000000000000000100;*/
//	b = a ^ b;		/*b = 00000000000000000000000000000010*/			
//					/*a = 00000000000000000000000000000110;*/
//	a = a ^ b;       /*a = 00000000000000000000000000000100;*/
//	printf("a=%d\t b=%d", a, b);
//	return 0;
//}


				/*获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列*/
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	int i = 0;
//	int ret = 0;
//	printf("奇数位为：");
//	for (i = 0;i <32;i+=2 )
//	{
//		ret = (num>>i) &1;
//		
//		printf("%d ", ret);
//	}
//	printf("\n");
//	printf("偶数位为：");
//	for (i = 1; i < 32; i += 2)
//	{
//		ret = ((num>>i) &1);
//		
//		printf("%d ", ret);
//	}
//	return 0;
//}


//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	int i = 31;
//	int ret = 0;					/*00000000000000000000000000000010     2*/
//	printf("偶数位为：");      
//	for (i = 31; i>=1; i-= 2)
//	{
//		ret = (num >> i) & 1;
//		printf("%d ", ret);
//	}
//	printf("\n");
//	printf("奇数位为：");
//	for (i = 30; i >=0; i-= 2)
//	{
//		ret = ((num >> i) & 1);
//		printf("%d ", ret);
//	}
//	return 0;
//}