#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//求Sn = a + aa + aaa + aaaa + aaaaa的前5项之和，其中a是一个数字，
//例如：2 + 22 + 222 + 2222 + 22222
//int main()
//{
//
//	int a = 0;
//	scanf("%d", &a);
//	int n = 0;
//	scanf("%d", &n);
//	int sum = a;
//	for (int i = 1; i < n; i++)
//	{
//		sum = sum + a * 10 + a;
//	}
//	printf("%d", sum);
//	return 0;
//}



//求出0～100000之间的所有“水仙花数”并输出。
//“水仙花数”是指一个n位数，其各位数字的n次方之和确好等于该数本身，如 : 153＝1 ^ 3＋5 ^ 3＋3 ^ 3，则153是一个“水仙花数”。

#include<math.h>
int main()
{
		
	
	for (int i = 0; i <= 100000; i++)
	{
		int n = 1;  //n表示几位数!
		int sum = 0;
		int tem = i;
		while (tem /= 10)
		{
			n++;
		}
		//位数已经正确表达出来！
		
		tem = i;
		while (tem)
		{
			sum+=pow(tem % 10, n);
			tem /= 10;
		}
		if (sum == i)
		{
			printf("%d ", sum);
		}
	}
}