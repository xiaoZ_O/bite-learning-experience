#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int Count_Digit(const int N, const int D)
{
    int arr[10] = { 0 };
    int n = N;
    int d = D;
    if (N < 0)
        n = -N;
    else n = N;
    if (n == 0)
    {
        return 1;
    }
    while (n)
    {
        arr[n % 10]++;
        n /= 10;
    }
    return arr[d];
}
int main()
{
    int N, D;

    scanf("%d %d", &N, &D);
    printf("%d\n", Count_Digit(N, D));
    return 0;
}

/* 你的代码将被嵌在这里 */