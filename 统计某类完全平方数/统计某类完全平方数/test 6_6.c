#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <math.h>
//int IsTheNumber(const int N)
//{
//    int n = N;
//    int arr[10] = { 0 };       //用来统计一个数的次数！
//    
//    for (int i = 1; i < n; i++)
//    {
//        if (pow(i, 2) == n)    //i为完全平方数！
//        {
//            while (n)
//            {
//                {
//                    arr[n % 10]++;
//                    n/= 10;
//                }
//            }
//        }
//    }
//    for (int i = 0; i < 9; i++)
//    {
//        if (arr[i] > 1)
//        {
//            return 1;
//        }
//    }
//    return 0;
//}

//int main()
//{
//    int n1, n2, i, cnt;
//
//    scanf("%d %d", &n1, &n2);
//    cnt = 0;
//    for (i = n1; i <= n2; i++) {
//        if (IsTheNumber(i))
//            cnt++;
//    }
//    printf("cnt = %d\n", cnt);
//
//    return 0;
//}

/* 你的代码将被嵌在这里 */






int IsTheNumber(const int N)
{
    int n = N;
    int arr[10] = { 0 };       //用来统计一个数的次数！
    int s = sqrt(N);
    if(s*s==N)
    {
            while (n)
            {
                {
                    arr[n % 10]++;
                    n /= 10;
                }
            }
    }
    for (int i = 0; i < 9; i++)
    {
        if (arr[i] > 1)
        {
            return 1;
        }
    }
    return 0;
}
int main()
{
    int n1, n2, i, cnt;

    scanf("%d %d", &n1, &n2);
    cnt = 0;
    for (i = n1; i <= n2; i++) {
        if (IsTheNumber(i))
            cnt++;
    }
    printf("cnt = %d\n", cnt);

    return 0;
}

//int main()
//{
//	int m = sqrt(5);
//	printf("%d", m);
//	//可以证明sqrt函数可以求出的平方根不一定正确！
//
//}








//统计个位数字
int Count_Digit(const int N, const int D)
{
    int arr[10] = { 0 };
    int n = N;
    int d = D;
    if (N < 0)
        n = -N;
    else n = N;
    if (n == 0)
    {
        return 1;
    }
    while (n)
    {
        arr[n % 10]++;
        n /= 10;
    }
    return arr[d];
}