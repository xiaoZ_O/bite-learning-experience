#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include<assert.h>
#include<string.h>
//int main()
//{
//    int i = 0;
//    int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//    for (i = 0; i <= 12; i++)
//    {
//        arr[i] = 0;
//        printf("hello bit\n");
//    }
//    return 0;
//}



				/*实现函数strlen*/
//int my_strlen(const char* ptr)
//{
//	int count = 0;
//	while (*ptr)
//	{
//		count++;
//		ptr++;
//	}
//	return count;
//}
//int main()
//{
//	char arr[] = "abcdef";
//	int len=my_strlen(arr);
//	printf("%d", len);
//	return 0;
//}


			/*strcpy函数*/
//int main()
//{
//	
//	char arr1[] = "hello world";		/*源数组*/
//	char arr2[20] = { 0 };				/*目的数组*/
//	strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	/*printf("%s\n", strcpy(arr2, arr1));*/
//	return 0;
//}

						//自己实现strcpy函数！


//char* my_strcpy(char* dest, const char* sour)
//{
//	char* ret = dest;
//	assert(dest!=NULL && sour!=NULL);
//	while (*dest++ = *sour++)
//		;
//	return ret;
//}
//int main()
//{
//
//	char arr1[] = "hello world";		/*源数组*/
//	char arr2[20] = { 0 };				/*目的数组*/
//	printf("%s",my_strcpy(arr2, arr1));
//	return 0;
//}


//调整数组使奇数全部都位于偶数前面。
//题目：
//输入一个整数数组，实现一个函数，
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//所有偶数位于数组的后半部分。

int main()
{
	int arr[10] = {0};
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		scanf("%d", &arr[i]);
	}
	for (i = 0; i < 10; i++)
	{
		if (arr[i] % 2 != 0)
		{
			printf("%d ", arr[i]);
		}
	}
	for (i = 0; i < 10; i++)
	{
		if (arr[i] % 2 == 0)
		{
			printf("%d ", arr[i]);
		}
	}
	return 0;
}