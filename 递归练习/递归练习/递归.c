#define _CRT_SECURE_NO_WARNINGS 1#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//递归求斐波那契数
//int fuc(int n)
//{
//	if (n<=2)
//	{
//		return 1;
//	}
//	else
//	{
//		return fuc(n - 1) + fuc(n - 2);
//	}
//}
//int main()
//{
//	int i = 0;
//	scanf("%d", &i);
//	int ret=fuc(i);
//	printf("%d", ret);
//	return 0;
//}
//		

					//非递归求斐波那契数
//int fun(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//	int i = 0;
//	if (n <= 2)
//	{
//		return c;
//	}
//	else
//	{
//		for (i = 3; i <=n; i++)
//		{
//			c = a + b;
//			a = b;
//			b = c;
//		}
//	}
//	return c;
//}
//int main()
//{
//	int i = 0;
//	scanf("%d", &i);
//	int ret = fun(i);
//	printf("%d", ret);
//	return 0;
//
//}

			//用递归实现一个数的n次方
//int fun(int i,int q)//i代表一个数，q代表幂次方
//{
//	if (q< 2)
//	{
//		return i;
//	}
//	else
//	{
//		return i * fun(i,q-1);
//	}
//}
//int main()
//{
//	int i = 0;
//	int q = 0;
//	scanf("%d%d", &i, &q);
//	int s = fun(i, q);
//	printf("%d", s);
//	return 0;
//
//}



//////写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
//////
//////例如，调用DigitSum(1729)，则应该返回1 + 7 + 2 + 9，它的和是19
//////
//////输入：1729，输出：19

//int DigitSum(int n)
//{
//	static int sum = 0;   /*根据前面打印数字函数，再添加一个求和变量，利用static改变其生命周期即可实现该功能实现*/
//	int q = 0;
//	if (n > 9)
//	{
//		DigitSum(n / 10);
//	}
//	q = n % 10;
//	sum = sum + q;
//	return sum;
//}
//int main()
//{
//	int i = 0;
//	int sum = 0;
//	scanf("%d", &i);
//	sum = DigitSum(i);
//	printf("%d", sum);
//	return 0;
//}
//







//编写一个函数 reverse_string(char* string)（递归实现）
//
//实现：将参数字符串中的字符反向排列，不是逆序打印。
//
//要求：不能使用C函数库中的字符串操作函数。
//
//比如 :
////
//char arr[] = "abcdef";
//
//
//逆序之后数组的内容变成：fedcba       

//void reverse_string(char* string)
//{
//	if (*string != '\0')
//	{
//		string++;
//		reverse_string(string);
//		printf("%c", *(string-1));
//	}
//}
//int main()
//{
//	char string[20];
//	scanf("%s", string);
//	printf("逆序之后数组为：");
//	reverse_string(string);
//	return 0;
//}









								////递归方式实现打印一个整数的每一位
//int  print(int n)
//{
//	static sum = 0;			/*使用static是为了让上次求出的sum值继续使用，若不使用static则每次sum都是以0重新分配，得不到我们的求和。*/
//	int q = 0;
//	if (n > 9)
//	{
//		print(n / 10);
//	}
//	q = n % 10;
//	printf("%d\t", q);
//	sum = sum + q;
//	return sum;
//}
//int main()
//{
//	int i = 0;
//	int sum = 0;
//	scanf("%d", &i);
//	sum = print(i);
//	printf("\n%d", sum);
//	return 0;
//}


//递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
					//1.递归
//int ret(int n)
//{
//	if (n > 1)
//	{
//		return n * ret(n - 1);
//	}
//	return 1;
//}
//int main()
//{
//	int i = 0;
//	int sum = 0;
//	scanf("%d", &i);
//	sum = ret(i);
//	printf("%d", sum);
//	return 0;
//}


									////2.非递归
////int ret(int n)
////{
////	int i = 1;
////	int sum = 1;
////	for (i = 1; i <= n; i++)
////	{
////		sum = sum * i;
////	}
////	return sum;
////}
////int main()
////{
////	int i = 0;
////	scanf("%d", &i);
////	int sum=ret(i);
////	printf("%d", sum);
////	return 0;
////}

							/*递归和非递归分别实现strlen*/
								//1.递归
//int my_strlen(char * p)
//{
//	if (* p =='\0')
//	{
//		return 0;
//	}
//	else
//	{
//		return 1 + my_strlen(p + 1);
//	}
//
//}
//int main()
//{
//	char arr[30];
//	scanf("%s", arr);   /* scanf默认以空格结束，若输入字符串中间有空格，则计算的只是空格前字符的个数*/
//	int len = my_strlen(arr);
//	printf("%d", len);
//	return 0;
//}


							////2.非递归
//int my_strlen(char* p)
//{
//	int count = 0;
//	while (*p != '\0')
//	{
//		if (*p != '\0')
//		{
//			count++;
//			p++;
//		}
//	}
//	return count ;
//}
//int main()
//{
//	char arr[30];
//	scanf("%s", arr);
//	int len = my_strlen(arr);
//	printf("%d", len);
//	return 0;
//}


